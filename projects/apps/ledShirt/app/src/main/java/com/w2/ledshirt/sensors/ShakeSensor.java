package com.w2.ledshirt.sensors;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;

import com.w2.ledshirt.FragmentText;
import com.w2.ledshirt.MainActivity;
import com.w2.ledshirt.TcpSend;

import java.util.ArrayList;

/**
 * Created by wouters on 10/4/15.
 */
public class ShakeSensor implements SensorEventListener {
    private SensorManager senSensorManager;
    private Sensor senAccelerometer;

    public ShakeSensor(Context c) {
        senSensorManager = (SensorManager) c.getSystemService(Context.SENSOR_SERVICE);
        senAccelerometer = senSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        senSensorManager.registerListener(this, senAccelerometer , SensorManager.SENSOR_DELAY_GAME);

    }

    private long lastUpdate = 0;
    private float last_x, last_y, last_z;
    private static final int SHAKE_THRESHOLD = 600;


    double prevPeakShake = -100;
    int trysShake = 0;
    int trysFall = 0;

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        Sensor mySensor = sensorEvent.sensor;
        if(MainActivity.mFragmentAll.mShakeDetect == null || !MainActivity.mFragmentAll.mShakeDetect.isChecked()) {
            return;
        }
        if (mySensor.getType() == Sensor.TYPE_ACCELEROMETER) {
            float x = sensorEvent.values[0];
            float y = sensorEvent.values[1];
            float z = sensorEvent.values[2];

            long curTime = System.currentTimeMillis();

            if ((curTime - lastUpdate) > 50) {
                long diffTime = (curTime - lastUpdate);
                lastUpdate = curTime;

                int brightval = FragmentText.sBrightness;
                if(brightval == 0) {
                    brightval = 15;
                }

                float speed = Math.abs(x + y + z - last_x - last_y - last_z)/ diffTime * 10000;

                if(speed > 300) {
                    prevPeakShake = speed -20;
                    trysShake = 0;
                    if(brightval != MainActivity.prevBrightness) {
                        ArrayList<Byte> b = new ArrayList<Byte>();
                        b.add((byte) brightval);
                        b.add((byte) 0);
                        b.add((byte) 0);
                        b.add((byte) 0);
                        MainActivity.prevBrightness = brightval;
                        new Thread(new TcpSend(-1, b, 0x05, 0)).start();
                    }

                } else {
                    brightval = 1;
                    trysFall++;
                    if (brightval != MainActivity.prevBrightness && trysFall > 2) {
                        MainActivity.prevBrightness = brightval;
                        ArrayList<Byte> b = new ArrayList<Byte>();
                        b.add((byte) brightval);
                        b.add((byte) 0);
                        trysFall = 0;
                        b.add((byte) 0);
                        b.add((byte) 0);
                        new Thread(new TcpSend(-1, b, 0x05, 0)).start();
                    }
                    trysShake++;
                }

                if(trysShake > 4) {
                    prevPeakShake = prevPeakShake - 10;
                }


                last_x = x;
                last_y = y;
                last_z = z;
            }
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }

}
