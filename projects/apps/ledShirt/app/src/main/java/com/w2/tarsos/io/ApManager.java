package com.w2.tarsos.io;

import android.content.Context;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiManager;

import java.io.BufferedReader;
import java.io.FileReader;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.regex.Pattern;

public class ApManager {
    public final static String SSID = "LedAccess";
    //check whether wifi hotspot on or off


    public static class IpInfo {
        public String mIp;
        int id = 0;
        String mMac;
        IpInfo(String ip, String mac, int i) {
            mIp = ip;
            id = i;
            mMac = mac;
        }
    }

    public static ArrayList<IpInfo> getClientList() {
        int macCount = 0;
        BufferedReader br = null;
        ArrayList<IpInfo> arr = new ArrayList<IpInfo>();
        try {
            br = new BufferedReader(new FileReader("/proc/net/arp"));
            String line;
            int c = 0;
            while ((line = br.readLine()) != null) {
                String[] splitted = line.split(" +");
                if (splitted != null ) {
                    // Basic sanity check
                    String mac = splitted[3];
                    if (mac.matches("..:..:..:..:..:..")) {
                        macCount++;
                        c++;
                        arr.add(new IpInfo(splitted[0], mac, c));
//                        System.out.println("Mac : "+ mac + " IP Address : "+splitted[0] );
//                        System.out.println("Mac_Count  " + macCount + " MAC_ADDRESS  "+ mac);


                    }

                }
            }
        } catch(Exception e) {

        }
        return arr;
    }

    public static boolean isApOn(Context context) {
        WifiManager wifimanager = (WifiManager) context.getSystemService(context.WIFI_SERVICE);
        try {
            Method method = wifimanager.getClass().getDeclaredMethod("isWifiApEnabled");
            method.setAccessible(true);


            return (Boolean) method.invoke(wifimanager);
        }
        catch (Throwable ignored) {}
        return false;
    }

    private static String quoteNonHex(String value, int... allowedLengths) {
        return isHexOfLength(value, allowedLengths) ? value : convertToQuotedString(value);
    }
    /**
     * Encloses the incoming string inside double quotes, if it isn't already quoted.
     * @param string the input string
     * @return a quoted string, of the form "input".  If the input string is null, it returns null
     * as well.
     */
    private static String convertToQuotedString(String string) {
        if (string == null || string.length() == 0) {
            return null;
        }
        // If already quoted, return as-is
        if (string.charAt(0) == '"' && string.charAt(string.length() - 1) == '"') {
            return string;
        }
        return '\"' + string + '\"';
    }

    /**
     * @param value input to check
     * @param allowedLengths allowed lengths, if any
     * @return true if value is a non-null, non-empty string of hex digits, and if allowed lengths are given, has
     *  an allowed length
     */
    private static final Pattern HEX_DIGITS = Pattern.compile("[0-9A-Fa-f]+");

    private static boolean isHexOfLength(CharSequence value, int... allowedLengths) {
        if (value == null || !HEX_DIGITS.matcher(value).matches()) {
            return false;
        }
        if (allowedLengths.length == 0) {
            return true;
        }
        for (int length : allowedLengths) {
            if (value.length() == length) {
                return true;
            }
        }
        return false;
    }
    private static WifiConfiguration changeNetworkCommon(String ssid) {
        WifiConfiguration config = new WifiConfiguration();
        config.allowedAuthAlgorithms.clear();
        config.allowedGroupCiphers.clear();
        config.allowedKeyManagement.clear();
        config.allowedPairwiseCiphers.clear();
        config.allowedProtocols.clear();
        // Android API insists that an ascii SSID must be quoted to be correctly handled.
        config.SSID = ssid; // quoteNonHex(ssid);
        return config;
    }

    public static WifiConfiguration changeNetworkWPA(WifiManager wifiManager, String ssid, String password) {
        WifiConfiguration config = changeNetworkCommon(ssid);
        // Hex passwords that are 64 bits long are not to be quoted.
        config.preSharedKey = password; //quoteNonHex(password, 64);
        //config.allowedAuthAlgorithms.set(WifiConfiguration.AuthAlgorithm.OPEN);
        config.allowedProtocols.set(WifiConfiguration.Protocol.WPA); // For WPA
        config.allowedProtocols.set(WifiConfiguration.Protocol.RSN); // For WPA2
        config.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.WPA_PSK);
        config.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.WPA_EAP);
        config.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.TKIP);
        config.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.CCMP);
        config.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.TKIP);
        config.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.CCMP);
        return config;
    }

    // toggle wifi hotspot on or off
    public static boolean configApState(Context context) {
        WifiManager wifimanager = (WifiManager) context.getSystemService(context.WIFI_SERVICE);
        WifiConfiguration conf = changeNetworkWPA(wifimanager, SSID, "test12345");
        try {
            // if WiFi is on, turn it off
            if(isApOn(context)) {
                wifimanager.setWifiEnabled(false);
            } else {
                wifimanager.setWifiEnabled(true);
            }

            Method method = wifimanager.getClass().getMethod("setWifiApEnabled", WifiConfiguration.class, boolean.class);
            method.invoke(wifimanager, conf, !isApOn(context));
            return true;
        }
        catch (Exception e) {
            e.printStackTrace();
            System.out.println("IO EXcept");
        }
        return false;
    }
} // end of class