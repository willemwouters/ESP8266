package com.w2.ledshirt;

import android.graphics.Color;
import android.util.Log;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;

/**
 * Created by wouters on 8/21/15.
 */
public class TcpSend implements Runnable{

    private static final int SERVERPORT = 8080;
    private ArrayList<Byte> mBuffer;
    private int len;
    private int rep;
    ByteBuffer bbuf;
    int speed = 25;
    int b = 0;
    boolean mRunning = false;
    private int mId = 500;

    ArrayList<ApManager.IpInfo> mIpList;
    public TcpSend(int id, ArrayList<Byte> data, int lenn, int repeat) {
        mBuffer = data;
        len = lenn;
        b = data.get(0);
        mRunning = true;
        mId = id;
        rep = repeat;
        bbuf = ByteBuffer.allocate(1000);
        mIpList = ApManager.getClientList();

    }

    public static String ipToString(int ip, boolean broadcast) {
        String result = new String();

        Integer[] address = new Integer[4];
        for(int i = 0; i < 4; i++)
            address[i] = (ip >> 8*i) & 0xFF;
        for(int i = 0; i < 4; i++) {
            if(i != 3)
                result = result.concat(address[i]+".");
            else result = result.concat("255.");
        }
        return result.substring(0, result.length() - 2);
    }

    public void stop() {
        mRunning = false;
    }

    public void setSpeed(int sp) {
        if(sp < 0) {
            sp = 0;
        }
        if(sp >= 50) {
            sp = 49;
        }
        Log.d("WW", "Setting speed to:" + (50 - sp));
        speed = 50 - sp;
    }
int frame = 0;
    public void run() {
        try {
            mRunning = true;

            while(mRunning) {
                DatagramSocket ds = new DatagramSocket();
                DatagramPacket dp;
                if(rep == 0) {
                    mRunning = false;
                }
                if(mId == -2) {
                    Log.v("ww", "refreshing changes");
                    bbuf.put((byte) 0xFE);
                }
                bbuf.put((byte) len);
                for (int i = 0; i < mBuffer.size(); i++) {
                    bbuf.put(mBuffer.get(i));
                }
                try {
                    Thread.sleep(speed * 5);
                    //Log.d("WW", "Waiting:" + (speed * 10));
                } catch (InterruptedException ex) {
                }

                if(mId == -1 || mId == -2) {
                    for(ApManager.IpInfo temp : mIpList) {
                        dp = new DatagramPacket(bbuf.array(), bbuf.position(), InetAddress.getByName(temp.mIp), SERVERPORT);
                        ds.setBroadcast(true);
                        ds.send(dp);
                    }
                } else {
                    if (speed > 2 || frame % 2 == 0) {
                        synchronized (MainActivity.mFragmentArray) {
                            Iterator it = MainActivity.mFragmentArray.entrySet().iterator();
                            while (it.hasNext()) {
                                Map.Entry pair = (Map.Entry) it.next();
                                MainActivity.FragmentArray temp = (MainActivity.FragmentArray) pair.getValue();
                                if ((mId == temp.id && mId != 0) || mId == 0 && temp.id != 0) {
                                    if (mId == temp.id && mId != 500 || (mId == 0 && !temp.frag.ISLOCKED && !temp.frag.isDisabled)) {
                                        dp = new DatagramPacket(bbuf.array(), bbuf.position(), InetAddress.getByName(temp.ip), SERVERPORT);
                                        ds.setBroadcast(true);
                                        ds.send(dp);
                                    }
                                }
                            }
                        }
                    }
                }
frame++;
                if(mRunning) {
                    mBuffer.clear();
                    bbuf.clear();
                    bbuf.rewind();
                    int count = 0;
                    FragmentText.set(FragmentText.yoffset, false);
                    FragmentText.yoffset++;
                    mBuffer.add((byte) b);
                    for (int z = 0; z < 8; z++) {
                        for (int i = 0; i < 15; i++) {
                            mBuffer.add((byte) Color.red(FragmentText.mTextViews.get(count).color));
                            mBuffer.add((byte) Color.green(FragmentText.mTextViews.get(count).color));
                            mBuffer.add((byte) Color.blue(FragmentText.mTextViews.get(count).color));
                            count++;
                        }
                    }
                }
                ds.close();
            }


        }catch (IOException ex) {

            ex.printStackTrace();
        }
    }

}
