package com.w2.ledshirt;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.MulticastSocket;
import java.net.SocketAddress;
import java.net.SocketException;
import java.util.ArrayList;

import android.util.Log;
class ReceiverThread implements Runnable
{

    MainActivity mCallback ;
    void setCallback(MainActivity ac) {
        mCallback = ac;
    }
    @Override
    public void run() {
        String text = null;

            text = null;
            int server_port = 9090;
            byte[] message = new byte[1500];
            DatagramPacket p = new DatagramPacket(message, message.length);
            DatagramSocket s = null;
            try{
                s = new DatagramSocket(server_port);
                s.setSoTimeout(5000);
            }catch (SocketException e) {
                e.printStackTrace();
                System.out.println("Socket excep");
            }
             while(true){

                try {
                    s.receive(p);
                    if(p.getLength() > 0 && p.getAddress() != null) {
                        if((Integer.valueOf(p.getData()[0]) & 0xFF) == 0xFE) {
                            Long id = Long.valueOf((Integer.valueOf(p.getData()[1]) << 24) | (Integer.valueOf(p.getData()[2]) << 16) | (Integer.valueOf(p.getData()[3]) << 8) | (Integer.valueOf(p.getData()[4])));
                            Log.d("WW", "Received alive message from" + p.getAddress().getHostAddress());
                            if(mCallback != null)
                                mCallback.onGetId(id, p.getAddress().getHostAddress().toString(), Integer.valueOf(p.getData()[5]));
                        }
                    }
                }catch (IOException e) {
                    e.printStackTrace();
                    mCallback.onGetId(Long.valueOf(0), "", 0);
                    System.out.println("IO EXcept");
                }


             }
    }
}

