package com.w2.ledshirt;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridLayout;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.w2.ambilwarna.AmbilWarnaDialog;
import com.w2.widget.CustomAdapter;
import com.w2.widget.QuickActionView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Random;

/**
 * Created by wouters on 8/21/15.
 */
public class FragmentText extends Fragment {
    public boolean ISLOCKED =false;
    class CustTextView {
        public TextView txt;
        public int color;
    }
    public ToggleButton mSoundDetect = null;
    public ToggleButton mShakeDetect = null;

    public boolean isDisabled = true;
    private FragmentText mContext;
    private boolean mVariant = false;
    public void setAllVariant(boolean v) {
        mVariant = v;
    }
    public static int yoffset   = 0;
    public static ArrayList<CustTextView> mTextViews = new ArrayList<CustTextView>();
    public static char arr2[][] = {     {0x10, 0x30, 0xFF, 0xA0},
                                        {0x10, 0x30, 0xFF, 0xA0},
                                        {0x10, 0x30, 0xFF, 0xA0},
                                        {0x10, 0x30, 0xFF, 0xA0}};

    public static char arr[][] = {   {0xFF, 0x80, 0x30, 0x20},
                                     {0x80, 0xFF, 0xA0, 0x30},
                                     {0x30, 0xA0, 0xFF, 0x80},
                                     {0x20, 0x30, 0xA0, 0xFF}};
    public static char mskon = 1;
    private static int mScale = 0;
    public int mId = 0;
    private String mName;
    public void setId(int i) {
        mId = i;
    }

    public static final int BUF_TEXT_SCOLL = 3;
    public static final int BUF_TEXT_EACH = 4;
    public static final int BUF_TEXT = 0;
    public static final int BUF_NONE = -1;
    public static final int BUF_ICON = 1;
    public static final int BUF_STREAM = 2;

    public static int sBrightness = 0;
    public static int sLastBuf = BUF_NONE;
    public static String sText = null;
    public static boolean sTextCaps = true;
    public static boolean sFlicker = false;
    public static int sFramerate = 0;
    public static int sLastIcon1 = 0;
    public static int sLastIcon2 = 0;
    public static int sLastIconSend = 0;
    public static int sLastIconAnim = 0;
    public static int sLastActiveBuffer = 1;
    public static int sIconEndColor1 = 1;
    public static int sIconEndColor2 = 1;


    private static void setScale(int sc) {
        mScale = sc;
    }
    public static int calcc(int count, int x, int i, int color) {
        x = x % 8;
        i = i % 14;
        int msk = 0;
        int f = count;
        float p = 255;
        float ret = (p * f) / 112;

        if(mskon == 1) {
            if(color == 0) {
                ret = ret * (i + x) % 510;
            }
            if(color == 1) {
                ret =  ret * i % 255;
            }
            if(color == 2) {
                ret =  ret * x % 255;
            }


            if(mScale == 0) {
                msk = arr[x % 4][i % 4];
            }  else {
                msk = arr2[x % 4][i % 4];
            }
           ret = ret * msk;
           ret = ret  / 255;
            if(ret > 255) {
                ret = 0xFF;
            }
            if(ret < 0) {
                ret = 0;
            }
        }
        int intret = (int) Math.floor(ret);
        return (int) intret;
    }


    public static void set(int xoffset, boolean update) {
        int count = 0;
        for (int x = 0; x < 8; x++) {
            for (int i = 0; i < 15; i++) {
                int c = Color.rgb(calcc(count, i, x+xoffset, 0), calcc(count,i, x+xoffset, 1), calcc(count,i, x+xoffset, 2));
                mTextViews.get(count).color = c;
                if(update) {
                    mTextViews.get(count).txt.setBackgroundColor(c);
                    mTextViews.get(count).txt.refreshDrawableState();
                }
                count++;
            }
        }
    }


    public void setDisabled(boolean v) {
        isDisabled = v;
        if(v) {
            getView().setVisibility(View.INVISIBLE);
        }else {
            getView().setVisibility(View.VISIBLE);
        }
    }
    public TcpSend lSend;
    public TcpSendFillAnim lSendFill;
    public TcpSendPixAnim lSendPix;
    public TcpSendRandomColor lSendRandomColor;

    ///////// POPUP HANDLER SCREEN ///////////////
    public void setPopupResult(int resultcode, Intent i) {
        if(resultcode == 2) {
            ArrayList<String> list = i.getStringArrayListExtra("list");
            boolean caps = i.getBooleanExtra("caps", false);
            String b = i.getStringExtra("string");
            int lazy = 0;
            int done = 0;
            for (String x : list) {
                    if (x.equals(" ") && done == 0) {
                        done = 1;
                    } else if(done == 0) {
                        lazy++;
                    }
                }
            int count = lazy -1; //MainActivity.mFragmentArray.size() - 3;
            int upcount = 0;
            for (String x : list) {
                if (x.equals(" ")) {
                    return;
                }
                Iterator it = MainActivity.mFragmentArray.entrySet().iterator();
                while (it.hasNext()) {
                    Map.Entry pair = (Map.Entry)it.next();
                    MainActivity.FragmentArray frag = (MainActivity.FragmentArray) pair.getValue();
                        FragmentText f = (FragmentText) frag.frag;
                        Log.v("ww", "maybe" + x + "=" + frag.name + "-");
                        if (frag.name.equals(x)) {
                            String tosend = "";
                            for(int bla = 0; bla < count; bla++) {
                                tosend = tosend + "~";
                            }
                            String tosenda = "";
                            for(int bla2 = 0; bla2 < upcount; bla2++) {
                                tosenda = tosenda + "~";
                            }
                            Log.v("ww", "yup");
                            if(frag.id != 0) {
                                Log.v("ww", "Sending" + frag.id + ":" + frag.name + "=" + tosend + b + tosenda);
                                Framedriver.setText(frag.id, tosend + b + tosenda, caps);
                                count = count - 1;
                                upcount++;
                            }
                            if(count < 0) {
                                return;
                            }
                        }
                    }


            }



        }
            if (i != null && resultcode == 1) {
                HashMap<Integer, String> listHits = new HashMap<Integer, String>();
                ArrayList<String> list = i.getStringArrayListExtra("list");
                String b = i.getStringExtra("string");
                if(b == null)
                    return;
                int count = 0;
                for (String x : list) {
                    if(x.equals(" ")) {
                       return;
                    }
                    Iterator it = MainActivity.mFragmentArray.entrySet().iterator();
                    while (it.hasNext()) {
                        Map.Entry pair = (Map.Entry)it.next();
                        MainActivity.FragmentArray frag = (MainActivity.FragmentArray) pair.getValue();


                        FragmentText f = (FragmentText) frag.frag;
                        Log.v("ww", "Item:" + x + "-" + frag.name);
                        if (frag.name.equals(x)) {
                            Log.v("ww", "HIT:" + x + "-" + frag.name);
                            if(count + 2 <= b.length()) {
                                Framedriver.setTextForward(frag.id, b.substring(count, count + 2), i.getBooleanExtra("caps", false));
                            } else {
                                Framedriver.setTextForward(frag.id, "  ", false);
                            }
                            count += 2;
                        }
                    }


                }
            }
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
    }

    private SnakeTimer snake = null;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView;
        mContext = this;
        if(mVariant == true) {
             rootView = inflater.inflate(R.layout.fragment_all, container, false);
        } else {
             rootView = inflater.inflate(R.layout.fragment_text, container, false);
        }
        mId = getArguments().getInt("id");
        mName = getArguments().getString("name");
//        FragmentText fragmentB = (FragmentText)getActivity()
//                .getSupportFragmentManager()
//                .findFragmentByTag(TabOfFragmentB);


        GridLayout gr = (GridLayout) rootView.findViewById(R.id.grid);
        int count = 0;
        for (int i = 0; i < 15; i++) {
            for(int x = 0; x < 8; x++) {
                TextView txt = new TextView(getActivity());
                txt.setText(String.valueOf(count));
                GridLayout.LayoutParams par = new GridLayout.LayoutParams();
                par.height = 100;
                par.width = 100;
                par.columnSpec = GridLayout.spec(i);
                par.rowSpec = GridLayout.spec(8 - x);
                //gr.addView(txt, count, par);
                CustTextView c = new CustTextView();
                c.txt = txt;
                mTextViews.add(c);
                count++;
            }
        }
        final EditText ed = (EditText) rootView.findViewById(R.id.textlbl);
        final ToggleButton senbtntxtcap = (ToggleButton) rootView.findViewById(R.id.senbtntxtcap);

        if(mVariant) {

            mSoundDetect = (ToggleButton) rootView.findViewById(R.id.btnSound);
            mShakeDetect = (ToggleButton) rootView.findViewById(R.id.btnShake);


            final LinearLayout snakecontrols = (LinearLayout) rootView.findViewById(R.id.snakecontrols);
            final ToggleButton btnSnake = (ToggleButton) rootView.findViewById(R.id.btnSnake);
            btnSnake.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(btnSnake.isChecked()) {
                        snake = new SnakeTimer();
                        snakecontrols.setVisibility(View.VISIBLE);
                        snake.setRunning(true);
                        new Thread(snake).start();
                    } else {
                        snakecontrols.setVisibility(View.INVISIBLE);
                        if(snake != null) {
                            snake.setRunning(false);
                        }
                    }
                }
            });

            Button btnUp = (Button) rootView.findViewById(R.id.btnUp);
            btnUp.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(snake != null)
                    snake.moveTo(SnakeTimer.UP);
                }
            });

            Button btnLeft = (Button) rootView.findViewById(R.id.btnLeft);
            btnLeft.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(snake != null)
                    snake.moveTo(SnakeTimer.LEFT);
                }
            });

            Button btnDown = (Button) rootView.findViewById(R.id.btnDown);
            btnDown.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(snake != null)
                    snake.moveTo(SnakeTimer.DOWN);
                }
            });

            Button btnRight = (Button) rootView.findViewById(R.id.btnRight);
            btnRight.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(snake != null)
                    snake.moveTo(SnakeTimer.RIGHT);
                }
            });





            Button btnRefresh = (Button) rootView.findViewById(R.id.btnRefresh);
            btnRefresh.setOnClickListener(new View.OnClickListener() {
              @Override
              public void onClick(View view) {
                  new Handler().postDelayed(new Runnable() {

                      @Override
                      public void run() {
                          int count = MainActivity.mFragmentArray.size() - 1;
                          if(count == 0){
                              return;
                          }
                          int c = 0;
                          int s = 0;
                          for(int i = 0; i < 20; i++) {
                              try {
                                  Thread.sleep(200);
                              } catch (InterruptedException e) {
                                  e.printStackTrace();
                              }

                              s = 0;
                              Iterator it = MainActivity.mFragmentArray.entrySet().iterator();
                              while (it.hasNext()) {
                                  Map.Entry pair = (Map.Entry) it.next();
                                  MainActivity.FragmentArray frag = (MainActivity.FragmentArray) pair.getValue();
                                  if(frag.id != 0) {
                                      if ((s + c) % count == 0) {
                                          Framedriver.sendIcon(frag.id, 2, Icons.getIcon(1));
                                      } else {
                                          Framedriver.setValue(frag.id, 2, 0x00, 0x00, 0x00);
                                      }
                                      s++;
                                  }

                              }
                             c++;
                          }

                          c = 0;
                          Iterator it = MainActivity.mFragmentArray.entrySet().iterator();
                          while (it.hasNext()) {
                              Map.Entry pair = (Map.Entry) it.next();
                              MainActivity.FragmentArray frag = (MainActivity.FragmentArray) pair.getValue();
                              Framedriver.setValue(0, 2, 0x00, 0x00, 0x00);
                          }

                          try {
                              Thread.sleep(100);
                          } catch (InterruptedException e) {
                              e.printStackTrace();
                          }

                          c = 0;
                          int r = (int) new Random().nextInt(count);
                          Log.v("WW", "random: r" + r);
                          it = MainActivity.mFragmentArray.entrySet().iterator();
                          while (it.hasNext()) {
                              Map.Entry pair = (Map.Entry) it.next();
                              MainActivity.FragmentArray frag = (MainActivity.FragmentArray) pair.getValue();
                              if(c == r) {
                                  try {
                                      Framedriver.sendIcon(frag.id, 2, Icons.getIcon(1));
                                      Thread.sleep(500);
                                      Framedriver.setValue(frag.id, 2, 0x00, 0x00, 0x00);
                                      Thread.sleep(500);
                                      Framedriver.sendIcon(frag.id, 2, Icons.getIcon(1));
                                      Thread.sleep(500);
                                      Framedriver.setValue(frag.id, 2, 0x00, 0x00, 0x00);
                                      Thread.sleep(500);
                                      Framedriver.sendIcon(frag.id, 2, Icons.getIcon(1));
                                      Thread.sleep(500);
                                      Framedriver.setValue(frag.id, 2, 0x00, 0x00, 0x00);
                                      Thread.sleep(500);
                                      Framedriver.sendIcon(frag.id, 2, Icons.getIcon(1));
                                      Thread.sleep(500);
                                      Framedriver.setValue(frag.id, 2, 0x00, 0x00, 0x00);
                                      Thread.sleep(500);
                                      Framedriver.sendIcon(frag.id, 2, Icons.getIcon(1));
                                  } catch (InterruptedException e) {
                                      e.printStackTrace();
                                  }


                              }
                              c++;
                          }


                      }

                  }, 1000);

              }
          });
            Button btnall = (Button) rootView.findViewById(R.id.senbtntxtall);
            btnall.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Log.d("ww", "Sending package");

                    ArrangeActivity nextFrag= new ArrangeActivity();
                    nextFrag.setCallback(mContext);
                    if(mId == 0) {
                        sLastBuf = BUF_TEXT;
                        sText = ed.getText().toString();
                        sTextCaps = senbtntxtcap.isChecked();
                    }
                    nextFrag.setOptions(senbtntxtcap.isChecked(), ed.getText().toString(), 2);
                    View v = mContext.getActivity().getCurrentFocus();
                    if (v != null) {
                        InputMethodManager imm = (InputMethodManager)mContext.getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                    }
                    getFragmentManager().beginTransaction()
                            .replace(R.id.containerlayout, nextFrag, "TEST")
                            .addToBackStack(null)
                            .commit();

                }
            });


            Button btnsettings = (Button) rootView.findViewById(R.id.settings);
            btnsettings.setOnClickListener(new View.OnClickListener() {
               @Override
               public void onClick(View view) {
                   Intent i = new Intent(getActivity(), AppPreferences.class);
                   startActivity(i);
               }
           });
            Button btnalleach = (Button) rootView.findViewById(R.id.senbtntxtalleach);
            btnalleach.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Log.d("ww", "Sending package");
                    View v = mContext.getActivity().getCurrentFocus();
                    if (v != null) {
                        InputMethodManager imm = (InputMethodManager)mContext.getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                    }
                    if(mId == 0) {
                        sLastBuf = BUF_TEXT_EACH;
                        sText = ed.getText().toString();
                        sTextCaps = senbtntxtcap.isChecked();
                    }
                    ArrangeActivity nextFrag= new ArrangeActivity();
                    nextFrag.setCallback(mContext);
                    nextFrag.setOptions(senbtntxtcap.isChecked(), ed.getText().toString(),1);
                    getFragmentManager().beginTransaction()
                            .replace(R.id.containerlayout, nextFrag, "TEST")
                            .addToBackStack(null)
                            .commit();

                    //startActivityForResult(intent, 0);

                }
            });



        }

        Button btn2 = (Button) rootView.findViewById(R.id.senbtntxt);
        btn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Framedriver.setText(mId, ed.getText().toString(), senbtntxtcap.isChecked());
                View v = mContext.getActivity().getCurrentFocus();
                if (v != null) {
                    if(mId == 0) {
                        sLastBuf = BUF_TEXT_SCOLL;
                        sText = ed.getText().toString();
                        sTextCaps = senbtntxtcap.isChecked();
                    }
                    InputMethodManager imm = (InputMethodManager)mContext.getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                }
            }
        });

        final ToggleButton lBtnFlicker = (ToggleButton) rootView.findViewById(R.id.senbtnflck);
        lBtnFlicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(mId == 0) {
                    sFlicker = lBtnFlicker.isChecked();
                }
                Framedriver.setFlicker(mId, lBtnFlicker.isChecked());
            }
        });


        Button lBtnColor = (Button) rootView.findViewById(R.id.choosecolor);
        lBtnColor.setOnClickListener(new View.OnClickListener() {
               @Override
               public void onClick(View view) {
                   AmbilWarnaDialog dialog = new AmbilWarnaDialog(getActivity(), Color.RED, new AmbilWarnaDialog.OnAmbilWarnaListener() {
                       @Override
                       public void onOk(AmbilWarnaDialog dialog, int color) {
                           Framedriver.setColorTextFront(mId, Color.red(color), Color.green(color), Color.blue(color));
                       }
                       @Override
                       public void onCancel(AmbilWarnaDialog dialog) {     }
                   });
                   dialog.show();
               }
           });


        Button lBtnColorBack = (Button) rootView.findViewById(R.id.choosecolorback);
        lBtnColorBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AmbilWarnaDialog dialog = new AmbilWarnaDialog(getActivity(), Color.RED, new AmbilWarnaDialog.OnAmbilWarnaListener() {
                    @Override
                    public void onOk(AmbilWarnaDialog dialog, int color) {
                        Framedriver.setColorTextBack(mId, Color.red(color), Color.green(color), Color.blue(color));
                    }
                    @Override
                    public void onCancel(AmbilWarnaDialog dialog) {     }
                });
                dialog.show();
            }
        });

        SeekBar seek = (SeekBar) rootView.findViewById(R.id.seekBar);
        seek.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                if(b) {
                    int frame = seekBar.getProgress();
                    if(mId == 0) {
                        sFramerate = frame;
                    }

                    Framedriver.setFramespeed(mId, frame);
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {  }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });

        SeekBar seekDim = (SeekBar) rootView.findViewById(R.id.seekBarDim);
        seekDim.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                if(b) {
                    if(mId == 0) {
                        sBrightness = seekBar.getProgress();
                    }
                    Framedriver.setBrightness(mId, seekBar.getProgress());
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {  }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });






        /// STUFF FOR ANIMATiONS
        final SeekBar seekPatternSpeed = (SeekBar) rootView.findViewById(R.id.seekBarPatternSpeed);
        seekPatternSpeed.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                if(!b) return;
                if (lSend != null)
                    lSend.setSpeed(seekPatternSpeed.getProgress());

                if (lSendFill != null) {
                    lSendFill.setSpeed(seekPatternSpeed.getProgress());
                }
                if (lSendPix != null) {
                    lSendPix.setSpeed(seekPatternSpeed.getProgress());
                }
                if (lSendRandomColor != null) {
                    lSendRandomColor.setSpeed(seekBar.getProgress());
                }

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {         }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {


            }
        });

        /// STUFF FOR ANIMATiONS
        final Spinner spinner = (Spinner) rootView.findViewById(R.id.spinner);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity(), R.array.effect_array,  android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        final ToggleButton senbtnpatt4 = (ToggleButton) rootView.findViewById(R.id.senbtnpatt4);
        senbtnpatt4.setOnClickListener(   new View.OnClickListener() {
            ArrayList<Byte> b;

            Thread t;
            @Override
            public void onClick(View view) {
                sLastBuf = BUF_STREAM;
                if (senbtnpatt4.isChecked()) {
                    b = new ArrayList<Byte>();
                    b.add((byte) 2);
                    if(lSendFill != null) {
                        lSendFill.stop();
                    }
                    if(lSendPix != null) {
                        lSendPix.stop();
                    }
                    if(lSendRandomColor != null) {
                        lSendRandomColor.stop();
                    }

                    if(lSend != null) {
                        lSend.stop();
                    }

                    Iterator it = MainActivity.mFragmentArray.entrySet().iterator();
                    while (it.hasNext()) {
                        Map.Entry pair = (Map.Entry)it.next();
                        MainActivity.FragmentArray frag = (MainActivity.FragmentArray) pair.getValue();
                            FragmentText f = (FragmentText) frag.frag;
                            if (mId == 0 && frag.id != 0) {
                                if(f != null && f.ISLOCKED == false) {
                                    if (f.getView() != null) {
                                        ToggleButton tog = (ToggleButton) f.getView().findViewById(R.id.senbtnpatt4);
                                        if(tog != null)
                                        tog.setChecked(false);
                                    }
                                    if (f.lSendFill != null) {
                                        f.lSendFill.stop();
                                    }
                                    if (f.lSendPix != null) {
                                        f.lSendPix.stop();
                                    }
                                    if (f.lSendRandomColor != null) {
                                        f.lSendRandomColor.stop();
                                    }

                                    if (f.lSend != null) {
                                        f.lSend.stop();
                                    }
                                }
                            } else if(mId != 0) {
                                if (f.mId == 0 && f.getView() != null && ISLOCKED == false) {
                                    ToggleButton tog = (ToggleButton) f.getView().findViewById(R.id.senbtnpatt4);
                                    if(tog.isChecked()) {
                                        senbtnpatt4.setChecked(false);
                                        return;
                                    }
                                }
                            }

                        }


                    if(spinner.getSelectedItemPosition() == 0) {
                        lSendFill = new TcpSendFillAnim(mId, b, 1, 1);
                        lSendFill.setMode(1);
                        lSendFill.setSpeed(seekPatternSpeed.getProgress());
                        t = new Thread(lSendFill);
                    } else if(spinner.getSelectedItemPosition() == 1) {
                        lSendPix = new TcpSendPixAnim(mId, b, 1, 1);
                        lSendPix.setMode(1);
                        lSendPix.setSpeed(seekPatternSpeed.getProgress());
                        t = new Thread(lSendPix);
                    } else if(spinner.getSelectedItemPosition() == 2){
                        lSendPix = new TcpSendPixAnim(mId, b, 1, 1);
                        lSendPix.setMode(0);
                        lSendPix.setSpeed(seekPatternSpeed.getProgress());
                        t = new Thread(lSendPix);
                    } else if(spinner.getSelectedItemPosition() == 3) {
                        lSend = new TcpSend(mId, b, 1, 1);
                        lSend.setSpeed(seekPatternSpeed.getProgress());
                        setScale(0);
                        t = new Thread(lSend);
                    } else if(spinner.getSelectedItemPosition() == 4) {
                        lSend = new TcpSend(mId, b, 1, 1);
                        lSend.setSpeed(seekPatternSpeed.getProgress());
                        setScale(1);
                        t = new Thread(lSend);
                    } else if(spinner.getSelectedItemPosition() == 5) {
                        lSendRandomColor = new TcpSendRandomColor(mId);
                        lSendRandomColor.setSpeed(seekPatternSpeed.getProgress());
                        lSendRandomColor.setSameColor(true);
                        t = new Thread(lSendRandomColor);
                    } else if(spinner.getSelectedItemPosition() == 6) {
                        lSendRandomColor = new TcpSendRandomColor(mId);
                        lSendRandomColor.setSpeed(seekPatternSpeed.getProgress());
                        t = new Thread(lSendRandomColor);
                    } else {
                        return;
                    }

                    t.setPriority(Thread.MAX_PRIORITY);
                    t.start();
                } else {


                    if(lSendFill != null) {
                        lSendFill.stop();
                    }
                    if(lSendPix != null) {
                        lSendPix.stop();
                    }
                    if(lSendRandomColor != null) {
                        lSendRandomColor.stop();
                    }

                    if(lSend != null) {
                        lSend.stop();
                    }
                }

            }
        } );


        if(mId != 0) {

            final ToggleButton lBtnLock = (ToggleButton) rootView.findViewById(R.id.btnlock);
            lBtnLock.setOnClickListener(   new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ISLOCKED = lBtnLock.isChecked();
                }
            } );
        }
        // ICONS ANIM TOGGLE
        final ToggleButton lToggleAnimation = (ToggleButton) rootView.findViewById(R.id.senbtnanim);
        lToggleAnimation.setOnClickListener(   new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                    Framedriver.setFlickerBuffer(mId, lToggleAnimation.isChecked());
                if(mId == 0)
                sLastIconAnim = lToggleAnimation.isChecked() ? 1 : 0;
            }
        } );


        Button btnChooseIcon = (Button) rootView.findViewById(R.id.btnChooseIcon);
        btnChooseIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                QuickActionView qa = QuickActionView.Builder( view );
                qa.setAdapter( new CustomAdapter( getActivity() ) );
                qa.setNumColumns(4);

                qa.setOnClickListener( new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick( DialogInterface dialog, int which ) {
                        dialog.dismiss();
                        if(mId == 0) {
                            sLastBuf = BUF_ICON;
                            sLastIcon1 = which;
                            sLastIconSend = 1;
                        }
                        if(which == Icons.END) {
                            AmbilWarnaDialog diag = new AmbilWarnaDialog(getActivity(), Color.RED, new AmbilWarnaDialog.OnAmbilWarnaListener() {
                                @Override
                                public void onOk(AmbilWarnaDialog dialog, int color) {
                                    Framedriver.setValue(mId, 1, Color.red(color), Color.green(color), Color.blue(color));
                                    sIconEndColor1 = color;
                                }
                                @Override
                                public void onCancel(AmbilWarnaDialog dialog) {     }
                            });
                            diag.show();
                            return;
                        }


                        if(which < 3) {
                            Framedriver.sendIcon(mId, 1, Icons.getIcon(which));
                        } else {
                            Log.v("WW", "Getting icon" + which );
                            Framedriver.sendSimpleIcon(mId, 1, Icons.getIcon(which));
                        }
                    }
                } );
                qa.show();
            }
        });



        Button btnChooseIcon2 = (Button) rootView.findViewById(R.id.btnChooseIcon2);
        btnChooseIcon2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                QuickActionView qa = QuickActionView.Builder( view );
                qa.setAdapter( new CustomAdapter( getActivity() ) );
                qa.setNumColumns(4);
                qa.setOnClickListener( new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick( DialogInterface dialog, int which ) {
                        dialog.dismiss();
                        if(mId == 0) {
                            sLastBuf = BUF_ICON;
                            sLastIcon2 = which;
                            sLastIconSend = 2;
                        }
                        if(which == Icons.END) {
                            AmbilWarnaDialog diag = new AmbilWarnaDialog(getActivity(), Color.RED, new AmbilWarnaDialog.OnAmbilWarnaListener() {
                                @Override
                                public void onOk(AmbilWarnaDialog dialog, int color) {
                                    Framedriver.setValue(mId, 2, Color.red(color), Color.green(color), Color.blue(color));
                                    sIconEndColor2 = color;
                                }
                                @Override
                                public void onCancel(AmbilWarnaDialog dialog) {     }
                            });
                            diag.show();
                            return;
                        }

                        if(which < 3) {
                            Framedriver.sendIcon(mId, 2, Icons.getIcon(which));
                        } else {
                            Framedriver.sendSimpleIcon(mId, 2, Icons.getIcon(which));
                        }
                    }
                } );
                qa.show();
            }
        });

        return rootView;
    }


}
