package com.w2.ledshirt;

import android.app.ActionBar;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.preference.PreferenceManager;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Toast;

import com.w2.ledshirt.adapter.TabsAdapter;
import com.w2.ledshirt.sensors.AudioSensor;
import com.w2.ledshirt.sensors.ShakeSensor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class MainActivity extends FragmentActivity  {

    public static int prevBrightness = 0;

    private static boolean wasopen = false;
    private MainActivity mContext;
    public static FragmentText mFragmentAll = null;
    private ActionBar bar = null;

    ViewPager mViewPager;
    TabsAdapter mTabsAdapter;



    public class FragmentArray {
        public FragmentText frag;
        public int id;
        public long alive = 0;
        public int timeout = 0;
        public String ip;
        public String name;
        FragmentArray(FragmentText f, int s, String n, String i) {
            frag = f;
            ip = i;
            id = s;
            name = n;
        }
    }
    public static HashMap<Integer, FragmentArray> mFragmentArrayPriv = new HashMap<Integer, FragmentArray>();
    public static Map<Integer, FragmentArray> mFragmentArray = java.util.Collections.synchronizedMap(mFragmentArrayPriv);



    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        mContext = this;
        // requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);


        LocalBroadcastManager.getInstance(this).registerReceiver(onNotice, new IntentFilter("Msg"));

        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        Map<String, ?> pr = pref.getAll();
        for (Map.Entry<String, ?> entry : pr.entrySet()) {
            Log.d("map values", entry.getKey() + ": " + entry.getValue().toString());
            if(isInteger(entry.getKey(),10))
                mNameMap.put(Integer.valueOf(entry.getKey()), entry.getValue().toString());
        }


        if(wasopen == false) {
            Toast.makeText(getApplicationContext(), "Starting up... Searching for other controllers", Toast.LENGTH_LONG).show();

            if (ApManager.isApOn(this)) {
                ApManager.configApState(this);
            }

            wifi = (WifiManager) getSystemService(Context.WIFI_SERVICE);
            if (wifi.isWifiEnabled() == false) {
                wifi.setWifiEnabled(true);
            }

            wifi.startScan();
            registerReceiver(mBroadcastReceiver, new IntentFilter(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION));
            wasopen = true;
        }

        mViewPager = new ViewPager(this);
        mViewPager.setId(View.generateViewId());
        setContentView(R.layout.activity_main);

        ViewGroup gr = (ViewGroup) findViewById(R.id.containerlayout);
        gr.addView(mViewPager);
        bar = getActionBar();
        bar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
        bar.setDisplayShowHomeEnabled(false);
        bar.setDisplayShowTitleEnabled(false);
        bar.setDisplayUseLogoEnabled(false);

        mTabsAdapter = new TabsAdapter(this, mViewPager);

        new AudioSensor();
        new ShakeSensor(this);


        Bundle args = new Bundle();
        String txt = "All";
        args.putString("name", txt);

        FragmentText frag = new FragmentText();

        addFragment(frag, 0, txt, null);
        mFragmentAll = frag;
        frag.setArguments(args);
        args.putInt("id", 0);
        frag.setRetainInstance(true);
        frag.setAllVariant(true);
        mTabsAdapter.addTab(bar.newTab().setText(txt),
                FragmentText.class, args);



        mTabsAdapter.mViewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                FragmentText frag = (FragmentText) mTabsAdapter.getItem(position);
                getActionBar().setSelectedNavigationItem(position);

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        if (savedInstanceState != null) {
            bar.setSelectedNavigationItem(savedInstanceState.getInt("tab", 0));
        }

    }




    public int addFragment(FragmentText frag, int i, String s, String ip) {
        synchronized(MainActivity.mFragmentArray) {
            FragmentArray f = new FragmentArray(frag, i, s, ip);
            f.alive = System.currentTimeMillis();
            Log.d("ww", "adding fragment" + s + f.ip);
            mFragmentArray.put(i, f);
            return i;
        }
    }
    public void removeFragment(FragmentText frag, int s) {
       // FragmentArray f = new FragmentArray(frag, s);
        synchronized(MainActivity.mFragmentArray) {
            Log.d("ww", "removing fragment" + s);
            Iterator it = mFragmentArray.entrySet().iterator();
            while (it.hasNext()) {
                Map.Entry pair = (Map.Entry) it.next();
                FragmentArray arr = (FragmentArray) pair.getValue();
                if (arr.id == s) {
                    it.remove(); // avoids a ConcurrentModificationException
                }
            }
        }
    }



    protected void onPause() {
        super.onPause();
    }

    protected void onResume() {
        SharedPreferences SP = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        String strUserName = SP.getString("username", "NA");
        boolean bAppUpdates = SP.getBoolean("applicationUpdates",false);
        String downloadType = SP.getString("downloadType","1");
        super.onResume();

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onDestroy() {
        if(ApManager.isApOn(this)) {
            Log.v("REMOVING CONNECTION", "gg");
            ApManager.configApState(this);
        }
        unregisterReceiver(mBroadcastReceiver);
        //senSensorManager.unregisterListener(this);

        super.onDestroy();
    }

    boolean doubleBackToExitPressedOnce = false;


    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();

            if(ApManager.isApOn(this)) {
                ApManager.configApState(this);
            }

            if (wifi.isWifiEnabled() == false) {
                wifi.setWifiEnabled(true);
            }
            android.os.Process.killProcess(android.os.Process.myPid());
            System.exit(1);
            return;
        }
        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce=false;
            }
        }, 1000);
    }


    //////// ROUTINE FOR CHECKING OTHER SERVICES/AP SERVICE ////////////
    private WifiManager wifi;
    BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver()
    {
        @Override
        public void onReceive(Context c, Intent intent)
        {
            boolean found = false;
            List<ScanResult> results = wifi.getScanResults();
            for(ScanResult temp : results) {
                Log.v("WW", "ssid:" + temp.SSID);
                if(temp.SSID.equals(ApManager.SSID)) {
                    found = true;
                }
            }
            if(found == true) {
                Toast.makeText(getApplicationContext(), "Other Servers already running", Toast.LENGTH_LONG).show();
                Handler h = new Handler();
                h.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if(ApManager.isApOn(getApplicationContext())) {
                            ApManager.configApState(getApplicationContext());
                        }
                        wifi = (WifiManager) getSystemService(Context.WIFI_SERVICE);
                        if (wifi.isWifiEnabled() == false) {
                            wifi.setWifiEnabled(true);
                        }
                        android.os.Process.killProcess(android.os.Process.myPid());
                        System.exit(1);
                    }
                }, 1000);

            } else {
                Toast.makeText(getApplicationContext(), "Starting Server", Toast.LENGTH_LONG).show();


                if(!ApManager.isApOn(getApplicationContext())) {
                    ApManager.configApState(getApplicationContext());
                }

                ScanIdThread s = new ScanIdThread();
                new Thread(s).start();

                ReceiverThread r = new ReceiverThread();
                r.setCallback(mContext);
                new Thread(r).start();

            }
        }
    };


    public static HashMap<Integer, String> mNameMap = new HashMap<>();


    //////// CALLBACK FOR KEEPALIVE WATCHDOG ////////////////////
    public void onGetId(Long id, String ip, int initcount) {
        boolean hit = false;
        int rem = 0;
        int intid =  id.intValue();
        synchronized(MainActivity.mFragmentArray) {
            Iterator it = mFragmentArray.entrySet().iterator();
            while (it.hasNext()) {
                Map.Entry pair = (Map.Entry) it.next();
                final FragmentArray temp = (FragmentArray) pair.getValue();
                int i = (Integer) pair.getKey();
                long diff = (System.currentTimeMillis() - temp.alive);
                if (temp.id == intid && intid != 0) {
                    Handler mHandler = new Handler(Looper.getMainLooper()) {
                        @Override
                        public void handleMessage(Message msg) {
                            for (int x = 0; x < TabsAdapter.mTabs.size(); x++) {
                                final TabsAdapter.TabInfo lTab =TabsAdapter.mTabs.get(x);
                                Bundle args = lTab.getArgs();
                                if (args.getInt("id")== temp.id) {
                                    getActionBar().getTabAt(x).setText(temp.name + " (@)");
                                    args.putString("name", temp.name + " (@)");
                                }
                            }

                            if(msg.what < 5) {
                                ScanIdThread.mShouldRefresh = true;
                            }
                            if(temp.frag.isDisabled) {
                                temp.frag.setDisabled(false);
                            }

                        }
                    };
                    Message msg = mHandler.obtainMessage(initcount);
                    mHandler.sendMessage(msg);

                    temp.alive = System.currentTimeMillis();
                    hit = true;
                } else if (diff > 5000) {
                    for (int x = 0; x < TabsAdapter.mTabs.size(); x++) {
                        final TabsAdapter.TabInfo lTab = TabsAdapter.mTabs.get(x);
                        Bundle args = lTab.getArgs();
                        if (args.getInt("id") != 0 && temp.id != 0 && !temp.frag.isDisabled) {

                            if (args.getInt("id") == temp.id && temp.id != 0) {
                                Log.v("ww", "We should remove" + diff + "-" + System.currentTimeMillis() + "-" + temp.alive);
                                Log.v("ww", "id" + args.getInt("id") + "-" + temp.id);
                                Handler mHandler = new Handler(Looper.getMainLooper()) {
                                    @Override
                                    public void handleMessage(Message msg) {
                                        temp.frag.setDisabled(true);
                                        final TabsAdapter.TabInfo lTab = TabsAdapter.mTabs.get(msg.what);
                                        Bundle args = lTab.getArgs();
                                        args.putString("name", temp.name + " (-)");
                                        getActionBar().getTabAt(msg.what).setText(temp.name + " (-)");
                                    }
                                };
                                Message msg = mHandler.obtainMessage(x);
                                mHandler.sendMessage(msg);
                                rem = 1;
                            }
                        }
                    }

                }
            }
        }

        if(hit == false && intid != 0) {
            Log.v("ww", "Not known");
            Handler mHandler = new Handler(Looper.getMainLooper()) {
                @Override
                public void handleMessage(Message msg) {
                    // Gets the image task from the incoming Message object.
                    Bundle args = new Bundle();
                    String ip = (String) msg.obj;
                    int intid = msg.what;
                    String name = String.valueOf(intid);
                    if (mNameMap.get(intid) != null) {
                        name = mNameMap.get(intid);
                    }
                    args.putString("name", name + " (@)");
                    FragmentText frag = new FragmentText();
                    int newid = addFragment(frag, intid, name, ip);

                    args.putInt("id", newid);
                    frag.setArguments(args);
                    frag.setRetainInstance(true);
                    Log.v("ww", "added" + ip);
                    mTabsAdapter.addTab(bar.newTab().setText( name), FragmentText.class, args);
                }

            };
            Message msg = mHandler.obtainMessage(intid, ip);
            mHandler.sendMessage(msg);

        }

    }


///////// RECEIVER FOR NOTIFICATION SERVICE ////////////////
    private BroadcastReceiver onNotice= new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            String pack = intent.getStringExtra("package");
            String title = intent.getStringExtra("title");
            String text = intent.getStringExtra("text");
            String subname = intent.getStringExtra("subname");

            if (FragmentText.sLastBuf == FragmentText.BUF_TEXT_SCOLL) {
                if (FragmentText.sText != null) {
                    Intent i = new Intent();
                    ArrayList<String> arr = new ArrayList<String>();
                    for(int x = 0;x < ArrangeActivity.mItemArray.size(); x++) {
                        if(subname != "" || subname.equals(ArrangeActivity.mItemArray.get(x).second)) {
                            arr.add(ArrangeActivity.mItemArray.get(x).second);
                        }
                    }
                    i.putStringArrayListExtra("list",arr);
                    i.putExtra("string", text);
                    i.putExtra("caps", FragmentText.sTextCaps);
                    MainActivity.mFragmentArray.get(0).frag.setPopupResult(0, i);
                }
            } else if (FragmentText.sLastBuf == FragmentText.BUF_TEXT_EACH) {
                if (FragmentText.sText != null) {
                    Intent i = new Intent();
                    ArrayList<String> arr = new ArrayList<String>();
                    for(int x = 0;x < ArrangeActivity.mItemArray.size(); x++) {
                        arr.add(ArrangeActivity.mItemArray.get(x).second);
                    }
                    i.putStringArrayListExtra("list",arr);
                    i.putExtra("string", text);
                    i.putExtra("caps", FragmentText.sTextCaps);
                    MainActivity.mFragmentArray.get(0).frag.setPopupResult(1, i);
                }
            } else {
                Framedriver.setText(0, text, FragmentText.sTextCaps);
            }

        }
    };



    public static boolean isInteger(String s, int radix) {
        if(s.isEmpty()) return false;
        for(int i = 0; i < s.length(); i++) {
            if(i == 0 && s.charAt(i) == '-') {
                if(s.length() == 1) return false;
                else continue;
            }
            if(Character.digit(s.charAt(i),radix) < 0) return false;
        }
        return true;
    }


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        //super.onSaveInstanceState(outState);
        outState.putInt("tab", getActionBar().getSelectedNavigationIndex());
    }



}
