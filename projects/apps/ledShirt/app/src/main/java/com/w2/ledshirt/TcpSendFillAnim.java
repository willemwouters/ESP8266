package com.w2.ledshirt;

import android.util.Log;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import java.util.Random;

/**
 * Created by wouters on 8/21/15.
 */
public class TcpSendFillAnim implements Runnable{

    private static final int SERVERPORT = 8080;
    private static final String SERVER_IP = "192.168.43.42";
    private ArrayList<Byte> mBuffer;
    private int len;
    private int mode = 0;
    private int rep;
    ByteBuffer bbuf;
    int speed = 25;
    int b = 0;
    boolean mRunning = false;
    public void setMode(int m) {
        mode = m;
    }
    ArrayList<ApManager.IpInfo> mIpList;

    class posabs {
        public int columncount;
        public int rowcount;
        public int r;
        public int g;
        public int b;

        posabs(int col, int row) {
            columncount = col;
            rowcount = row;
            Random rand = new Random();

              r = rand.nextInt(255) + 1;
              g = rand.nextInt(255) + 1;
              b = rand.nextInt(255) + 1;

        }

    };
    ArrayList<posabs> posfilled = new ArrayList<posabs>();
    private int mId = 500;
    TcpSendFillAnim(int id, ArrayList<Byte> data, int lenn, int repeat) {
        mBuffer = data;
        len = lenn;
        mId = id;
        b = data.get(0);
        mRunning = true;
        rep = repeat;
        bbuf = ByteBuffer.allocate(1000);
        mIpList = ApManager.getClientList();
    }

    public static String ipToString(int ip, boolean broadcast) {
        String result = new String();

        Integer[] address = new Integer[4];
        for(int i = 0; i < 4; i++)
            address[i] = (ip >> 8*i) & 0xFF;
        for(int i = 0; i < 4; i++) {
            if(i != 3)
                result = result.concat(address[i]+".");
            else result = result.concat("255.");
        }
        return result.substring(0, result.length() - 2);
    }

    public void stop() {

        mRunning = false;
    }

    public void setSpeed(int sp) {
        if(sp < 0) {
            sp = 0;
        }
        if(sp >= 50) {
            sp = 49;
        }
        Log.d("WW", "Setting speed to:" + (50 - sp));
        speed = 50 - sp;
    }
    private boolean rempos(int columncount, int rowcounter) {
        boolean ret = false;

        Iterator<posabs> CrunchifyIterator = posfilled.iterator();
        while (CrunchifyIterator.hasNext()) {
            posabs temp = CrunchifyIterator.next();
            if(temp.columncount == columncount && temp.rowcount == rowcounter) {
                posfilled.remove(temp);
                ret = true;
                return ret;
            }

            System.out.println();
        }

        for (posabs temp : posfilled) {

        }
        return ret;
    }

    private posabs getpos(int columncount, int rowcounter) {
        boolean ret = false;
        Iterator<posabs> CrunchifyIterator = posfilled.iterator();
        while (CrunchifyIterator.hasNext()) {
            posabs temp = CrunchifyIterator.next();
            if(temp.columncount == columncount && temp.rowcount == rowcounter) {
                return temp;
            }
        }
        return null;
    }
    private int forward = 1;
    private int rowcounter = 0;
    private int columncount = 0;
    public void run() {
        try {
            mRunning = true;
            int frame = 0;
            int spec = 0;
            int sw = 0;
            while(mRunning) {
                DatagramSocket ds = new DatagramSocket();
                DatagramPacket dp;

                if(rep == 0) {
                    mRunning = false;
                }
                bbuf.put((byte) len);
                for (int i = 0; i < mBuffer.size(); i++) {
                    bbuf.put(mBuffer.get(i));
                }
                try {
                    Thread.sleep(speed);
                    //Log.d("WW", "Waiting:" + (speed * 10));
                } catch (InterruptedException ex) {
                }

                if(speed > 5 || frame % 5 == 0) {
                    synchronized (MainActivity.mFragmentArray) {
                        Iterator it = MainActivity.mFragmentArray.entrySet().iterator();
                        while (it.hasNext()) {
                            Map.Entry pair = (Map.Entry) it.next();
                            MainActivity.FragmentArray temp = (MainActivity.FragmentArray) pair.getValue();
                            if ((mId == temp.id && mId != 0) || mId == 0 && temp.id != 0) {
                                if (mId == temp.id && mId != 500 || (mId == 0 && !temp.frag.ISLOCKED && !temp.frag.isDisabled)) {
                                    dp = new DatagramPacket(bbuf.array(), bbuf.position(), InetAddress.getByName(temp.ip), SERVERPORT);
                                    ds.setBroadcast(true);
                                    ds.send(dp);
                                }
                            }
                        }
                    }
                }
               // Log.d("WW", "Send out message" + String.valueOf(bbuf.array()) + " len:" + mBuffer.size());

                frame++;
                if(mRunning) {
                    mBuffer.clear();
                    bbuf.clear();
                    bbuf.rewind();
                    int count = 0;
                    FragmentText.set(FragmentText.yoffset, false);
                    FragmentText.yoffset++;
                    mBuffer.add((byte) b);

                    if(forward == 1) {
                        if ((columncount % 15 == 0 && columncount != 0) || getpos(columncount, rowcounter) != null) {
                            posfilled.add(new posabs(columncount - 1, rowcounter));

                            if (posfilled.size() == 14 * 8) {
                                rempos(columncount, rowcounter);
                                forward = 0;
                                columncount = 0;
                            } else {
                                rowcounter++;
                                columncount = 0;
                            }
                        }
                        if (rowcounter == 8 && forward == 1) {
                            rowcounter = 0;
                        }
                        if(forward == 1)
                        columncount++;
                    } else {
                        posfilled.remove(posfilled.size()-1);
                        if ((columncount % 14 == 0 || columncount == 0) || getpos(columncount, rowcounter) == null)
                        {
                            if(posfilled.size() == 0) {
                                forward = 1;
                                rowcounter--;

                            } else {
                                rowcounter--;
                            }
                        }

                        columncount--;
                        if (rowcounter == 0) {
                            rowcounter = 8;
                        }
                        if(columncount < 0) {
                            columncount = 14;
                        }
                    }


                    for (int z = 0; z < 15; z++) {
                        for (int i = 0; i < 8; i++) {
                            if((rowcounter == i && columncount == z) || getpos(z, i) != null) {
                                int  r = 0xFF;
                                int  g = 0xFF;
                                int  b = 0xFF;
                                if(getpos(z,i) != null) {
                                      r = getpos(z,i).r;
                                      g = getpos(z,i).g;
                                      b = getpos(z,i).b;
                                }
                                mBuffer.add((byte) r);
                                mBuffer.add((byte) g);
                                mBuffer.add((byte) b);

                            } else {
                                mBuffer.add((byte) 0x00);
                                mBuffer.add((byte) 0x00);
                                mBuffer.add((byte) 0x00);
                            }


                        }
                    }

                }
                ds.close();
            }

            Log.d("WW", "Ended");


        }catch (IOException ex) {

            ex.printStackTrace();
        }
    }

}
