package com.w2.ledshirt.sensors;

import com.w2.ledshirt.FragmentText;
import com.w2.ledshirt.MainActivity;
import com.w2.ledshirt.TcpSend;
import com.w2.tarsos.io.AudioDispatcherFactory;

import java.util.ArrayList;

import be.tarsos.dsp.AudioDispatcher;
import be.tarsos.dsp.AudioEvent;
import be.tarsos.dsp.AudioProcessor;
import be.tarsos.dsp.SilenceDetector;

/**
 * Created by wouters on 10/4/15.
 */
public class AudioSensor implements AudioProcessor {
    AudioDispatcher dispatcher;
    SilenceDetector silenceDetector;
    private int threshold = 20;



    public AudioSensor() {
        int sampleRate = 44100;
        int bufferSize = 7168;
        int overlap = 5;
        dispatcher = AudioDispatcherFactory.fromDefaultMicrophone(sampleRate, bufferSize, overlap);
        silenceDetector = new SilenceDetector(threshold,false);
        dispatcher.addAudioProcessor(silenceDetector);
        dispatcher.addAudioProcessor(this);

        // run the dispatcher (on a new thread).
        Thread t = new Thread(dispatcher,"Audio dispatching");
        t.setPriority(Thread.MAX_PRIORITY);
        t.start();

    }
    @Override
    public boolean process(AudioEvent audioEvent) {
        handleSound();
        return false;
    }

    @Override
    public void processingFinished() {

    }



    double prevPeak = -100;
    int trys = 0;
    private void handleSound(){
        if(MainActivity.mFragmentAll.mSoundDetect == null || !MainActivity.mFragmentAll.mSoundDetect.isChecked()) {

            return;
        }

        int brightval = FragmentText.sBrightness;
        if(brightval == 0) {
            brightval = 15;
        }
        double n = silenceDetector.currentSPL();
        if(prevPeak < n) {
            prevPeak = n -1;
            trys = 0;
            if (brightval != MainActivity.prevBrightness) {
                MainActivity.prevBrightness = brightval;
                ArrayList<Byte> b = new ArrayList<Byte>();
                b.add((byte) brightval);
                b.add((byte) 0);
                b.add((byte) 0);
                b.add((byte) 0);
                new Thread(new TcpSend(-1, b, 0x05, 0)).start();
            }

        } else
        if(prevPeak < (n + 4)) {
            prevPeak = n + 1;
            trys = 0;
            brightval =  brightval / 2;
            if (brightval != MainActivity.prevBrightness) {
                MainActivity.prevBrightness = brightval;
                ArrayList<Byte> b = new ArrayList<Byte>();
                b.add((byte) brightval);
                b.add((byte) 0);
                b.add((byte) 0);
                b.add((byte) 0);
                new Thread(new TcpSend(-1, b, 0x05, 0)).start();
            }

        }
        else {
            brightval = 1;
            if (brightval != MainActivity.prevBrightness) {
                MainActivity.prevBrightness = brightval;
                ArrayList<Byte> b = new ArrayList<Byte>();
                b.add((byte) brightval);
                b.add((byte) 0);
                b.add((byte) 0);
                b.add((byte) 0);
                new Thread(new TcpSend(-1, b, 0x05, 0)).start();
            }
            trys++;
        }

        if(trys > 2) {
            prevPeak = prevPeak - 1;
        }

    }
}
