package com.w2.ledshirt;

import java.util.ArrayList;

/**
 * Created by wouters on 9/12/15.
 */
public class SnakeTimer implements Runnable {

    public static char drugs[] = {
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    };

    private final static int ALL_DOTS = 120;

    private final int x[] = new int[ALL_DOTS];
    private final int y[] = new int[ALL_DOTS];

    private int dots;
    private int apple_x;
    private int apple_y;

    public final static int RIGHT = 1;
    public final static int LEFT = 2;
    public final static int UP = 3;
    public final static int DOWN = 4;

    private static boolean leftDirection = false;
    private static boolean rightDirection = true;
    private static boolean upDirection = false;
    private static boolean downDirection = false;

    private boolean mRunning = true;
    private boolean mLost = false;

    public void setRunning(boolean r) {
        mRunning = r;
    }

    public void moveTo(int key) {

        if ((key == LEFT) && (!rightDirection)) {
            leftDirection = true;
            upDirection = false;
            downDirection = false;
        }

        if ((key == RIGHT) && (!leftDirection)) {
            rightDirection = true;
            upDirection = false;
            downDirection = false;
        }

        if ((key == UP) && (!downDirection)) {
            upDirection = true;
            rightDirection = false;
            leftDirection = false;
        }

        if ((key == DOWN) && (!upDirection)) {
            downDirection = true;
            rightDirection = false;
            leftDirection = false;
        }
    }
    private void move() {

        for (int z = dots; z > 0; z--) {
            x[z] = x[(z - 1)];
            y[z] = y[(z - 1)];
        }

        if (leftDirection) {
            x[0] -= 1;
        }

        if (rightDirection) {
            x[0] += 1;
        }

        if (downDirection) {
            y[0] -= 1;
        }

        if (upDirection) {
            y[0] += 1;
        }
    }


    private void initGame() {
        mLost = false;
        dots = 3;
        mDelay = 0;
        rightDirection = true;
        upDirection = false;
        downDirection = false;
        leftDirection = false;
        for (int z = 0; z < dots; z++) {
            x[z] = 3 - z;
            y[z] = 3;
        }
        locateApple();

    }


    private void checkCollision() {

        for (int z = dots; z > 0; z--) {
            if ((z > 4) && (x[0] == x[z]) && (y[0] == y[z])) {
                mLost = true;
            }
        }

        if (y[0] >= 7) {
            mLost = true;
        }

        if (y[0] < 1) {
            mLost = true;
        }

        if (x[0] >= 14) {
            mLost = true;
        }

        if (x[0] < 1) {
            mLost = true;
        }




    }

    private void checkApple() {

        if ((x[0] == apple_x) && (y[0] == apple_y)) {
            dots++;
            locateApple();
            if(mDelay < 170) {
                mDelay = mDelay + 20;
            }
        }
    }


    private void locateApple() {

        int r = (int) (Math.random() * 13) + 1;
        apple_x = ((r * 1));

        r = (int) (Math.random() * 5) + 1;
        apple_y = ((r * 1));
    }
    private int mDelay = 0;
    @Override
    public void run() {
        initGame();

        while(mRunning) {
            if(mDelay < 80) {
                mDelay = mDelay + 1;
            }

            try {
                Thread.sleep(300 - mDelay);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            move();
            checkApple();
            checkCollision();
            ArrayList<Byte> b = new ArrayList<Byte>();
            b.add((byte) 2);
            for(int column = 0; column < 15; column++) {
                for(int rows = 0; rows < 8; rows++) {

                    int hit = 0;



                    for (int z = 0; z < dots; z++) {
                            if(x[z] == column && y[z] == rows && hit == 0) {
                                if(z == 0) {
                                    b.add((byte) 0xA0);
                                    b.add((byte) 0x30);
                                    b.add((byte) 0xFF);
                                } else {
                                    b.add((byte) 0xFF);
                                    b.add((byte) 0x80);
                                    b.add((byte) 0xFF);
                                }
                                hit = 1;
                            }
                    }

                    if(column == apple_x && rows == apple_y && hit == 0) {
                        b.add((byte) 0xFF);
                        b.add((byte) 0x00);
                        b.add((byte) 0x00);
                        hit = 1;
                    }

                    if((column == 0 || rows == 0 || rows == 7 || column == 14) && hit == 0) {
                        b.add((byte) 0xFF);
                        b.add((byte) 0xFF);
                        b.add((byte) 0xFF);
                        hit = 1;
                    }
                    if(hit == 0) {
                        b.add((byte) 0x00);
                        b.add((byte) 0x00);
                        b.add((byte) 0x00);
                    }
                }
             }
            new Thread(new TcpSend(0, b, Framedriver.REG_STREAMWRITE, 0)).start();



            if(mLost == true) {
                Framedriver.setValue(0, 2, 0xFF, 0x00, 0x00);
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                initGame();
            }

        }



    }
}
