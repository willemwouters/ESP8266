package com.w2.ledshirt;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.util.Pair;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.woxthebox.draglistview.DragItem;
import com.woxthebox.draglistview.DragListView;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;

public class ArrangeActivity extends Fragment {
    DragListView mDragListView;
    private String mString;
    private int mCode;
    private boolean mCaps;
    private Fragment mContext;
    public static ArrayList<Pair<Long,String>> mItemArray;
/*
    Intent i = getIntent();
    mString = i.getStringExtra("string");
    mCaps = i.getBooleanExtra("caps", false);
*/



    public void setOptions(boolean caps, String s, int code) {
        mString = s;
        mCaps = caps;
        mCode = code;
    }
    FragmentText mFragmentParent;
    public void setCallback(FragmentText f) {
        mFragmentParent = f;
    }
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_arrange, container, false);
        mDragListView = (DragListView) view.findViewById(R.id.drag_list_view);
        mContext = this;
        mDragListView.getRecyclerView().setVerticalScrollBarEnabled(true);
        mDragListView.setDragListListener(new DragListView.DragListListener() {
            @Override
            public void onItemDragStarted(int position) {
                //Toast.makeText(mDragListView.getContext(), "Start - position: " + position, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onItemDragEnded(int fromPosition, int toPosition) {
                if (fromPosition != toPosition) {
                    //Toast.makeText(mDragListView.getContext(), "End - position: " + toPosition, Toast.LENGTH_SHORT).show();
                }
            }
        });

        if(mItemArray == null || mItemArray.size() < 1 || mItemArray.size() != MainActivity.mFragmentArray.size()) {
            mItemArray = new ArrayList<Pair<Long, String>>();
            synchronized (MainActivity.mFragmentArray) {
                Iterator it = MainActivity.mFragmentArray.entrySet().iterator();
                while (it.hasNext()) {
                    Map.Entry pair = (Map.Entry) it.next();
                    MainActivity.FragmentArray temp = (MainActivity.FragmentArray) pair.getValue();
                    if(temp.id != 0) {
                        mItemArray.add(new Pair<Long, String>(Long.valueOf(temp.id), temp.name));
                    }
                }
            }

            mItemArray.add(new Pair<Long, String>(Long.valueOf(500), " "));

        }

        mDragListView.setLayoutManager(new LinearLayoutManager(getActivity()));
        final ItemAdapter  listAdapter = new ItemAdapter(mItemArray, R.layout.row, R.id.handler, false);
        mDragListView.setAdapter(listAdapter, true);
        mDragListView.setCanDragHorizontally(false);
        mDragListView.setCustomDragItem(new MyDragItem(getActivity(), R.layout.row));


        Button btncancel = (Button) view.findViewById(R.id.cancel);
        btncancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mContext.getFragmentManager().beginTransaction().remove(mContext).commit();
            }
        });



        Button btn = (Button) view.findViewById(R.id.send);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ArrayList<String> arr = new ArrayList<String>();
                for(int i = 0; i < listAdapter.getItemCount(); i++) {
                    Log.v("ww", "ww " + listAdapter.getItemList().get(i).second);
                    arr.add(listAdapter.getItemList().get(i).second);

                }
                Intent i = new Intent();
                i.putStringArrayListExtra("list",arr);
                i.putExtra("string", mString);
                i.putExtra("caps", mCaps);
                mFragmentParent.setPopupResult(mCode, i);
                mContext.getFragmentManager().beginTransaction().remove(mContext).commit();

            }
        });


        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        //((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("List and Grid");
    }


    private void setupListRecyclerView() {

    }


    private static class MyDragItem extends DragItem {

        public MyDragItem(Context context, int layoutId) {
            super(context, layoutId);
        }

        @Override
        public void onBindDragView(View clickedView, View dragView) {
            CharSequence text = ((TextView) clickedView.findViewById(R.id.text)).getText();
            ((TextView) dragView.findViewById(R.id.text)).setText(text);
            //dragView.setBackgroundColor(dragView.getResources().getColor(R.color.list_item_background));
        }
    }
}
    /*

*/