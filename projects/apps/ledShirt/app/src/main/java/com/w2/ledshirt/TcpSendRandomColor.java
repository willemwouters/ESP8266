package com.w2.ledshirt;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;

/**
 * Created by wouters on 8/21/15.
 */
public class TcpSendRandomColor implements Runnable{
    int speed = 25;
    boolean mRunning = false;
    private int mId = 500;
    ArrayList<ApManager.IpInfo> mIpList;
    TcpSendRandomColor(int id) {
        mRunning = true;
        mId = id;
        mIpList = ApManager.getClientList();

    }

    public void stop() {
        mRunning = false;
    }
    private boolean mSameColor = false;
    public void setSameColor(boolean t) {
        mSameColor = t;
    }
    public void setSpeed(int sp) {
        if(sp < 0) {
            sp = 0;
        }
        if(sp >= 50) {
            sp = 49;
        }
        speed = 50 - sp;
    }
int frame;
    public void run() {
        while(mRunning) {
            try {
                Thread.sleep(speed *20);
                //Log.d("WW", "Waiting:" + (speed * 10));
            } catch (InterruptedException ex) {
            }
            if (speed > 3 || frame % 3 == 0) {
                synchronized (MainActivity.mFragmentArray) {
                    Iterator it = MainActivity.mFragmentArray.entrySet().iterator();
                    int c = 0;
                    int r = 0;
                    int g = 0;
                    int b = 0;

                    while (it.hasNext()) {
                        Map.Entry pair = (Map.Entry) it.next();
                        MainActivity.FragmentArray temp = (MainActivity.FragmentArray) pair.getValue();
                        if ((mId == temp.id && mId != 0) || mId == 0 && temp.id != 0) {
                            if (mId == temp.id && mId != 500 || (mId == 0 && !temp.frag.ISLOCKED && !temp.frag.isDisabled)) {
                                if(! (mSameColor && c != 0)) {
                                     r = (int) (Math.random() * 255);
                                     g = (int) (Math.random() * 255);
                                     b = (int) (Math.random() * 255);
                                }
                                Framedriver.setValue(temp.id, 2, r, g, b);
                            }
                        }
                        c++;
                    }
                }
            }
            frame++;
        }
    }

}
