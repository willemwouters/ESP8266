package com.w2.ledshirt;

import android.util.Log;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;

/**
 * Created by wouters on 8/21/15.
 */
public class TcpSendPixAnim implements Runnable{

    private static final int SERVERPORT = 8080;
    private static final String SERVER_IP = "192.168.43.42";
    private ArrayList<Byte> mBuffer;
    private int len;
    private int mode = 0;
    private int rep;
    ByteBuffer bbuf;
    int speed = 25;
    int b = 0;
    boolean mRunning = false;
    public void setMode(int m) {
        mode = m;
    }
    private int mId = 500;
    ArrayList<ApManager.IpInfo> mIpList;

    TcpSendPixAnim(int id, ArrayList<Byte> data, int lenn, int repeat) {
        mBuffer = data;
        len = lenn;
        mId = id;
        b = data.get(0);
        mRunning = true;
        rep = repeat;
        bbuf = ByteBuffer.allocate(1000);
        mIpList = ApManager.getClientList();
    }

    public static String ipToString(int ip, boolean broadcast) {
        String result = new String();

        Integer[] address = new Integer[4];
        for(int i = 0; i < 4; i++)
            address[i] = (ip >> 8*i) & 0xFF;
        for(int i = 0; i < 4; i++) {
            if(i != 3)
                result = result.concat(address[i]+".");
            else result = result.concat("255.");
        }
        return result.substring(0, result.length() - 2);
    }

    public void stop() {

        mRunning = false;
    }

    public void setSpeed(int sp) {
        if(sp < 0) {
            sp = 0;
        }
        if(sp >= 50) {
            sp = 49;
        }
        Log.d("WW", "Setting speed to:" + (50 - sp));
        speed = 50 - sp;
    }
    private int rowcounter = 0;
    private int columncount = 0;
    int frame = 0;
    public void run() {
        try {
            mRunning = true;

            int spec = 0;
            int sw = 0;
            while(mRunning) {
                DatagramSocket ds = new DatagramSocket();
                DatagramPacket dp;

                if(rep == 0) {
                    mRunning = false;
                }
                bbuf.put((byte) len);
                for (int i = 0; i < mBuffer.size(); i++) {
                    bbuf.put(mBuffer.get(i));
                }
                try {
                    Thread.sleep(speed * 5);
                    //Log.d("WW", "Waiting:" + (speed * 10));
                } catch (InterruptedException ex) {
                }
                try {
                    if (speed > 3 || frame % 3 == 0) {

                        synchronized (MainActivity.mFragmentArray) {
                            Iterator it = MainActivity.mFragmentArray.entrySet().iterator();
                            while (it.hasNext()) {
                                Map.Entry pair = (Map.Entry) it.next();
                                MainActivity.FragmentArray temp = (MainActivity.FragmentArray) pair.getValue();
                                if ((mId == temp.id && mId != 0) || mId == 0 && temp.id != 0) {
                                    if (mId == temp.id && mId != 500 || (mId == 0 && !temp.frag.ISLOCKED && !temp.frag.isDisabled)) {
                                        dp = new DatagramPacket(bbuf.array(), bbuf.position(), InetAddress.getByName(temp.ip), SERVERPORT);
                                        ds.setBroadcast(true);
                                        ds.send(dp);
                                    }
                                }
                            }
                        }
                    }
                }
                catch (UnknownHostException e) {

                }
               // Log.d("WW", "Send out message" + String.valueOf(bbuf.array()) + " len:" + mBuffer.size());
                frame++;

                if(mRunning) {
                    mBuffer.clear();
                    bbuf.clear();
                    bbuf.rewind();
                    int count = 0;
                    FragmentText.set(FragmentText.yoffset, false);
                    FragmentText.yoffset++;
                    mBuffer.add((byte) b);
                    if(mode == 1) {
                        if (rowcounter % 8 == 0 && rowcounter != 0) {
                            columncount++;
                            ;
                            rowcounter = 0;
                        }
                        if (columncount == 15) {
                            columncount = 0;
                        }

                        rowcounter++;
                    } else {

                        if(rowcounter % 8 == 0 && spec == 0 && sw == 0) {
                            columncount++;
                            if (columncount % 2 == 1) {
                                rowcounter = 7;
                                spec = 1;
                            } else {
                                rowcounter = 0;
                                spec = 1;
                            }
                        }
                        if(columncount == 15) {
                            columncount = 0;
                        }
                        if(columncount % 2 == 1) {
                            if(spec != 1) {
                                sw = 0;
                                rowcounter--;
                            } else {
                                spec = 0;
                                sw = 1;
                            }
                        } else {
                            if(spec != 1) {
                                sw = 0;
                                rowcounter++;
                            } else {
                                spec = 0;
                                sw = 1;
                            }

                        }


                    }
                    for (int z = 0; z < 15; z++) {
                        for (int i = 0; i < 8; i++) {
                            if(rowcounter == i && columncount == z) {
                                mBuffer.add((byte) 0xFF);
                                mBuffer.add((byte) 0xFF);
                                mBuffer.add((byte) 0xFF);
                                count++;
                            } else {
                                mBuffer.add((byte) 0x00);
                                mBuffer.add((byte) 0x00);
                                mBuffer.add((byte) 0x00);
                            }


                        }
                    }

                }
                ds.close();
            }

            Log.d("WW", "Ended");


        }catch (IOException ex) {

            ex.printStackTrace();
        }
    }

}
