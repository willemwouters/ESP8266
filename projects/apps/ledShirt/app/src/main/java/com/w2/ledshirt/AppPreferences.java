package com.w2.ledshirt;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.EditTextPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;
import android.preference.PreferenceScreen;
import android.preference.SwitchPreference;
import android.util.Log;

import java.util.Iterator;
import java.util.Map;

public class AppPreferences extends PreferenceActivity implements Preference.OnPreferenceClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        PreferenceScreen screen = getPreferenceManager().createPreferenceScreen(this);
        //MainActivity.mFragmentArray
        Iterator it = MainActivity.mFragmentArray.entrySet().iterator();

        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        Map<String, ?> pr = pref.getAll();
        for (Map.Entry<String, ?> entry : pr.entrySet()) {
            Log.d("map values", entry.getKey() + ": " + entry.getValue().toString());
            if(!entry.getKey().equals("Clear")) {
                EditTextPreference p = new EditTextPreference(this);
                p.setText(entry.getKey());
                p.setKey(entry.getKey());
                p.setTitle(entry.getValue().toString());
                p.setDefaultValue(entry.getValue().toString());
                screen.addPreference(p);
            }
        }

        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry) it.next();
            MainActivity.FragmentArray temp = (MainActivity.FragmentArray) pair.getValue();

            if(pref.getString(String.valueOf(temp.id), "NA").equals("NA")) {
                EditTextPreference p = new EditTextPreference(this);
                p.setText(String.valueOf(temp.id));
                p.setKey(String.valueOf(temp.id));
                p.setDefaultValue(temp.name);
                p.setTitle(temp.name);
                screen.addPreference(p);
            }

        }

        SwitchPreference p = new SwitchPreference(this);
        p.setKey("Clear");
        p.setTitle("Clear");
        p.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
                pref.edit().clear().commit();
                Log.v("WW", "CLEAARRR");
                return false;
            }
        });
        screen.addPreference(p);


        setPreferenceScreen(screen);

    }

    @Override
    public boolean onPreferenceClick(Preference preference) {

        Log.v("WW", "Clearing all" + preference.getKey());
        return false;
    }
}