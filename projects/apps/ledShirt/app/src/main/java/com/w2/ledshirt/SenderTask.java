package com.w2.ledshirt;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;

import android.os.AsyncTask;
import android.util.Log;

public class SenderTask extends AsyncTask<Integer, Void, Void>  {
	

	    DatagramSocket client_socket;
	    private static boolean mAck = false;
		@Override
		protected Void doInBackground(Integer... p) {
			
	
			 try {
				 String sbuf = "                                       ";
				   byte[] buf = sbuf.getBytes();
				   buf[0] = 'A';
					buf[1] = 'P';
					buf[2] = ':';
					buf[3] = 'W';
					buf[4] = 'R';
					buf[5] = 'I';
					buf[6] = 'T';
					buf[7] = 'E';
					buf[8] = ':';
					buf[9] = p[0].byteValue();
					buf[10] = ':';
					buf[11] = p[1].byteValue();
					buf[12] = p[2].byteValue();
					buf[13] = p[3].byteValue();
					
				try {
					client_socket= new DatagramSocket();
				    InetAddress IPAddress =  InetAddress.getByName("224.0.0.1");
			       DatagramPacket send_packet = new DatagramPacket(buf, buf.length, IPAddress, 8000);
			       Log.d("WILLEM", "Sending package");
			       client_socket.send(send_packet);   
			       Log.d("WILLEM", "Send package");
			       int retries = 0;
			       while(mAck == false) {
			    	   for(int i = 0; i < 20 && mAck == false; i++) {
			    		   Thread.sleep(10);
			    	   }
			    	   if(mAck == false) {
			    		   Log.d("WILLEM", "First package failed, lets assume its lost, try again");
			    		   client_socket.send(send_packet);
			    	   }
			    	   retries++;
			    	   if(retries > 0) {
			    		   mAck = true;
			    	   }
			       }
			       mAck =false;
			       client_socket.close();
				} catch (SocketException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					client_socket.close();
				} catch (UnknownHostException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					client_socket.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					client_socket.close();
				}
		           
		        } catch (Exception e) {
		            client_socket.close();
		        }
			return null;
		}

			

	
}
