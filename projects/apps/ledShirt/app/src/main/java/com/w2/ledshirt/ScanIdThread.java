package com.w2.ledshirt;

import android.content.Intent;
import android.graphics.Color;

import java.util.ArrayList;

class ScanIdThread implements Runnable {

    public static boolean mShouldRefresh = false;

    @Override
    public void run() {

        while (true) {

            ArrayList<Byte> d = new ArrayList<Byte>();
            d.add((byte) 0);
            d.add((byte) 0);
            new Thread(new TcpSend(-1, d, 0xFF, 0)).start();


            try {
                Thread.sleep(2400);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if (mShouldRefresh) {
                if (FragmentText.sFramerate != 0) {
                    Framedriver.setFramespeed(-2, FragmentText.sFramerate);
                }

                if (FragmentText.sBrightness != 0) {
                    Framedriver.setBrightness(-2, FragmentText.sBrightness);
                }

                //Log.v("WW", "scanning");

                if (FragmentText.sFlicker == true) {
                    Framedriver.setFlicker(-2, true);
                }

                if (FragmentText.sLastBuf == FragmentText.BUF_ICON) {

                    int lasticon = 0;
                    int iconsend = 0;

                    int lastendcolor = 0;
                    if (FragmentText.sLastIconAnim == 1) {
                        Framedriver.setFlickerBuffer(0, true);
                    }

                    if (FragmentText.sLastIconSend == 2) {
                        iconsend = 1;
                        lasticon = FragmentText.sLastIcon1;
                        lastendcolor = FragmentText.sIconEndColor1;
                    } else if (FragmentText.sLastIconSend == 1) {
                        lasticon = FragmentText.sLastIcon2;
                        lastendcolor = FragmentText.sIconEndColor2;
                        iconsend = 2;
                    }
                    if (lasticon < 3) {
                        Framedriver.sendIcon(-2, iconsend, Icons.getIcon(lasticon));
                    } else if (lasticon == Icons.END) {
                        Framedriver.setValue(-2, iconsend, Color.red(lastendcolor), Color.green(lastendcolor), Color.blue(lastendcolor));
                    } else {
                        Framedriver.sendSimpleIcon(-2, iconsend, Icons.getIcon(lasticon));
                    }


                    if (FragmentText.sLastIconSend == 2) {
                        lasticon = FragmentText.sLastIcon2;
                        iconsend = 2;
                        lastendcolor = FragmentText.sIconEndColor2;
                    } else if (FragmentText.sLastIconSend == 1) {
                        lasticon = FragmentText.sLastIcon1;
                        lastendcolor = FragmentText.sIconEndColor1;
                        iconsend = 1;
                    }
                    if (lasticon < 3) {
                        Framedriver.sendIcon(-2, iconsend, Icons.getIcon(lasticon));
                    } else if (lasticon == Icons.END) {
                        Framedriver.setValue(-2, iconsend, Color.red(lastendcolor), Color.green(lastendcolor), Color.blue(lastendcolor));
                    } else {
                        Framedriver.sendSimpleIcon(-2, iconsend, Icons.getIcon(lasticon));
                    }

                } else if (FragmentText.sLastBuf == FragmentText.BUF_TEXT) {
                    if (FragmentText.sText != null) {
                        Framedriver.setText(-2, FragmentText.sText, FragmentText.sTextCaps);
                    }
                } else if (FragmentText.sLastBuf == FragmentText.BUF_TEXT_SCOLL) {
                    if (FragmentText.sText != null) {
                        Intent i = new Intent();
                        ArrayList<String> arr = new ArrayList<String>();
                        for(int x = 0;x < ArrangeActivity.mItemArray.size(); x++) {
                            arr.add(ArrangeActivity.mItemArray.get(x).second);
                        }
                        i.putStringArrayListExtra("list",arr);
                        i.putExtra("string", FragmentText.sText);
                        i.putExtra("caps", FragmentText.sTextCaps);
                        MainActivity.mFragmentArray.get(0).frag.setPopupResult(0, i);
                    }
                } else if (FragmentText.sLastBuf == FragmentText.BUF_TEXT_EACH) {
                    if (FragmentText.sText != null) {
                        Intent i = new Intent();
                        ArrayList<String> arr = new ArrayList<String>();
                        for(int x = 0;x < ArrangeActivity.mItemArray.size(); x++) {
                            arr.add(ArrangeActivity.mItemArray.get(x).second);
                        }
                        i.putStringArrayListExtra("list",arr);
                        i.putExtra("string", FragmentText.sText);
                        i.putExtra("caps", FragmentText.sTextCaps);
                        MainActivity.mFragmentArray.get(0).frag.setPopupResult(1, i);
                    }
                }
                mShouldRefresh = false;
            }

        }
    }
}


