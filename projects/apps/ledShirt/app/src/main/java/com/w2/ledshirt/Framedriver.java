package com.w2.ledshirt;

import java.util.ArrayList;

/**
 * Created by wouters on 9/5/15.
 */
public class Framedriver {

    public final static int REG_TEXTWRITE = 0x00;
    public final static int REG_STREAMWRITE = 0x01;
    public final static int REG_FLICKERMODE = 0x02;
    public final static int REG_FLICKERBUFFERMODE = 0x03;
    public final static int REG_FRAMESPEED = 0x04;
    public final static int REG_BRIGHTNESS = 0x05;
    private final static int REG_SETVAL = 0x07;
    private final static int REG_FONTCOLORFRONT = 0x08;
    private final static int REG_FONTCOLORBACK = 0x09;


    private static int sFrontColorR = 0xFF;
    private static int sFrontColorG = 0xFF;
    private static int sFrontColorB = 0xFF;

    private static void sendSimpleVal(int id, int val, int type) {
        ArrayList<Byte> b = new ArrayList<Byte>();
        b.add((byte) val);
        b.add((byte) 0);
        b.add((byte) 0);
        b.add((byte) 0);
        new Thread(new TcpSend(id, b, type, 0)).start();
    }

    public static void setTextForward(int id, String text, boolean upper) {
        ArrayList<Byte> b = new ArrayList<Byte>();
        int count = 0;
        b.add((byte) 0); // scoll disabled
        for (int x = 0; x < text.length(); x++) {
            byte r = 0;
            if(upper) {
                r = (byte) text.toUpperCase().charAt(x);
            } else {
                r = (byte) text.charAt(x);
            }
            b.add(r);
        }
        new Thread(new TcpSend(id, b, REG_TEXTWRITE, 0)).start();
    }

    public static void setText(int id, String text, boolean upper) {
        ArrayList<Byte> b = new ArrayList<Byte>();
        int count = 0;
        b.add((byte) 1); // scoll enabled
        b.add((byte) ' ');
        for (int x = 0; x < text.length(); x++) {
            byte r = 0;
            if(upper) {
                r = (byte) text.toUpperCase().charAt(x);
            } else {
                r = (byte) text.charAt(x);
            }
            b.add(r);
        }
        new Thread(new TcpSend(id, b, REG_TEXTWRITE, 0)).start();
    }

    public static void setValue(int id, int buffer, int r, int g, int b) {
        ArrayList<Byte> buf = new ArrayList<Byte>();
        buf.add((byte) buffer);
        buf.add((byte) r);
        buf.add((byte) g);
        buf.add((byte) b);
        new Thread(new TcpSend(id, buf, REG_SETVAL, 0)).start();
    }
    public static void setFlicker(int id, boolean val) {
        ArrayList<Byte> b = new ArrayList<Byte>();
        if (val) {
            b.add((byte) 0);
        } else {
            b.add((byte) 1);
        }
        b.add((byte) 0);
        b.add((byte) 0);
        b.add((byte) 0);
        new Thread(new TcpSend(id, b, REG_FLICKERMODE, 0)).start();
    }

    public static void setFlickerBuffer(int id, boolean val) {
        ArrayList<Byte> b = new ArrayList<Byte>();
        if (val) {
            b.add((byte) 0);
        } else {
            b.add((byte) 1);
        }
        b.add((byte) 0);
        b.add((byte) 0);
        b.add((byte) 0);
        new Thread(new TcpSend(id, b, REG_FLICKERBUFFERMODE, 0)).start();
    }

    public static void setColorTextBack(int id, int r, int g, int b) {
        ArrayList<Byte> buf = new ArrayList<Byte>();
        buf.add((byte) r);
        buf.add((byte) g);
        buf.add((byte) b);
        new Thread(new TcpSend(id, buf, REG_FONTCOLORBACK, 0)).start();
    }

    public static void setColorTextFront(int id, int r, int g, int b) {
        ArrayList<Byte> buf = new ArrayList<Byte>();
        buf.add((byte) r);
        sFrontColorR = r;
        buf.add((byte) g);
        sFrontColorG = g;
        buf.add((byte) b);
        sFrontColorB = b;
        new Thread(new TcpSend(id, buf, REG_FONTCOLORFRONT, 0)).start();
    }

    public static void setFramespeed(int id, int speed) {
        sendSimpleVal(id, speed+1, REG_FRAMESPEED);
    }


    public static void setBrightness(int id, int bright) {
        if (bright == 0) {
            bright = 1;
        }
        sendSimpleVal(id, bright, REG_BRIGHTNESS);
    }

    public static void sendIcon(int id, int buf, char [] arr) {
        ArrayList<Byte> b = new ArrayList<Byte>();
        int count = 0;
        b.add((byte) buf);
        for (int x = 0; x < 15; x++) {
            for (int i = 0; i < 8; i++) {
                b.add((byte) arr[count]);
                count++;
                b.add((byte) arr[count]);
                count++;
                b.add((byte) arr[count]);
                count++;
                if(count == arr.length) {
                    count = 0;
                }
            }
        }
        new Thread(new TcpSend(id, b, REG_STREAMWRITE, 0)).start();
    }

    public static void sendSimpleIcon(int id, int buf, char [] arr) {
        if(arr == null) {
            return;
        }
        ArrayList<Byte> b = new ArrayList<Byte>();
        int count = 0;
        b.add((byte) buf);
        for (int x = 0; x < 15; x++) {
            for (int i = 0; i < 8; i++) {
                if(arr[count] == 0xFF) {
                    b.add((byte) sFrontColorR);
                    b.add((byte) sFrontColorG);
                    b.add((byte) sFrontColorB);
                } else {
                    b.add((byte) arr[count]);
                    b.add((byte) arr[count]);
                    b.add((byte) arr[count]);
                }
                count++;
                if(count == arr.length) {
                    count = 0;
                }
            }
        }
        new Thread(new TcpSend(id, b, REG_STREAMWRITE, 0)).start();
    }


}
