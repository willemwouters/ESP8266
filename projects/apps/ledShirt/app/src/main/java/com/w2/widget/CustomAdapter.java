package com.w2.widget;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.w2.ledshirt.Icons;
import com.w2.ledshirt.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by wouters on 9/9/15.
 */
public class CustomAdapter  extends BaseAdapter {

        static final int[] ICONS = new int[]{

                R.drawable.emptybeericon,
                R.drawable.beericon,
                R.drawable.hearticon,
                R.drawable.boobsicon,
                R.drawable.caticon,
                R.drawable.fingericon,
                R.drawable.penisicon,
                R.drawable.drugsicon,
                R.drawable.likeicon,
                R.drawable.colorpickericon,
                R.drawable.empty,
                R.drawable.empty,
        };

        LayoutInflater mLayoutInflater;
        List<ActionItemText> mItems;

        public CustomAdapter( Context context ) {
            mLayoutInflater = (LayoutInflater) context.getSystemService( Context.LAYOUT_INFLATER_SERVICE );

            mItems = new ArrayList<ActionItemText>();
            int total = ICONS.length;

            for( int i = 0; i < total; i++ ) {
                ActionItemText item = new ActionItemText( context, String.valueOf(i), ICONS[ i ] );
                mItems.add( item );
            }
        }


        @Override
        public int getCount() {
            return mItems.size();
        }

        @Override
        public Object getItem( int arg0 ) {
            return mItems.get( arg0 );
        }

        @Override
        public long getItemId( int arg0 ) {
            return arg0;
        }

        @Override
        public View getView( int position, View arg1, ViewGroup arg2 ) {
            View view = mLayoutInflater.inflate( R.layout.action_item, arg2, false );

            ActionItemText item = (ActionItemText) getItem( position );

            ImageView image = (ImageView) view.findViewById( R.id.image );

            TextView text = (TextView) view.findViewById( R.id.title );


            if(Integer.valueOf(item.getTitle()) > Icons.END) {
                image.setVisibility(View.INVISIBLE);
                text.setVisibility(View.INVISIBLE);
                view.setVisibility(View.INVISIBLE);
            } else {
                image.setImageDrawable( item.getIcon() );

            }
            text.setText( item.getTitle() );

            return view;
        }


}
