package com.w2.ledshirt;


import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.service.notification.NotificationListenerService;
import android.service.notification.StatusBarNotification;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RemoteViews;
import android.widget.TextView;

import java.util.ArrayList;


public class NotificationService extends NotificationListenerService {

    Context context;

    @Override

    public void onCreate() {

        super.onCreate();
        context = getApplicationContext();

    }

    private static void extractViewType(ArrayList<View> outViews, Class<TextView> viewtype, View source) {
        if (ViewGroup.class.isInstance(source)) {
            ViewGroup vg = (ViewGroup) source;
            for (int i = 0; i < vg.getChildCount(); i++) {
                extractViewType(outViews, viewtype, vg.getChildAt(i));

            }
        } else if(viewtype.isInstance(source)) {
            outViews.add(source);
        }
    }

    public static ArrayList<String> extractTextFromNotification(Service service, RemoteViews view) {
        LayoutInflater inflater = (LayoutInflater) service.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        ArrayList<String> result = new ArrayList<String>();
        if (view == null) {
            Log.d("ww", "View is empty");
            return null;
        }
        try {
            int layoutid = view.getLayoutId();
            ViewGroup localView = (ViewGroup) inflater.inflate(layoutid, null);
            view.reapply(service.getApplicationContext(), localView);
            ArrayList<View> outViews = new ArrayList<View>();
            extractViewType(outViews, TextView.class, localView);
            for (View  ttv: outViews) {
                TextView tv = (TextView) ttv;
                String txt = tv.getText().toString();
                if (!TextUtils.isEmpty(txt)) {
                    result.add(txt);
                }
            }
        } catch (Exception e) {
            Log.d("ww", "FAILED to load notification " + e.toString());
            Log.wtf("ww", e);
            return null;
            //notification might have dissapeared by now
        }
        Log.d("ww", "Return result" + result);
        return result;
    }

    @Override
    public void onNotificationPosted(StatusBarNotification sbn) {


        String pack = sbn.getPackageName();
        String ticker = sbn.getNotification().tickerText.toString();



        Log.i("Package",pack);
        Log.i("Ticker",ticker);
        Intent msgrcv = new Intent("Msg");
        msgrcv.putExtra("package", pack);
        msgrcv.putExtra("ticker", ticker);
        if(Build.VERSION.SDK_INT >= 19 ){
            Bundle extras = sbn.getNotification().extras;
            String text = ""; // = extras.getCharSequence("android.text").toString();

            RemoteViews r = sbn.getNotification().bigContentView;
            ArrayList<String> ar = extractTextFromNotification(this, r);
            for(String s : ar) {
                if(s.length() > 5 && s.substring(0, 4).toUpperCase().equals("LED:")) {
                    Log.i("ww", "Latest:" + s.substring(4) + "-");
                    text = s.substring(4);
                }
            }
            if(!text.equals("")) {
                String title = extras.getString("android.title");
                Log.i("Title", title);
                msgrcv.putExtra("title", title);
                msgrcv.putExtra("text", text);
                msgrcv.putExtra("subname", "");//);
                Log.v("ww", "subname=" +  text.substring(0, text.indexOf(" ")));
                String ns = Context.NOTIFICATION_SERVICE;
                NotificationManager nMgr = (NotificationManager) context.getSystemService(ns);
                nMgr.cancel(sbn.getId());
                nMgr.cancelAll();
                LocalBroadcastManager.getInstance(context).sendBroadcast(msgrcv);
            }


        }




    }

    @Override

    public void onNotificationRemoved(StatusBarNotification sbn) {
        Log.i("Msg","Notification Removed");

    }
}
