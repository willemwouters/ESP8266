package com.w2.ledshirt;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.MulticastSocket;

import android.util.Log;

public class ReceiverThread implements Runnable {
	
	public interface  OnMessageReceived
	{
		public  void OnReceive(String data);
	} 
	private OnMessageReceived mClb = null;
    public static final int PORT_NO = 8000;
    //public static final String GROUP_ADDR = "225.0.0.1";
   public static final int DGRAM_LEN = 1024;
   // public static final String WHO_IS = "Who is?";
    //public static final int TIME_TO_LIVE = 1;
    public void SetCallback(OnMessageReceived cb) {
    	mClb = cb;
    }
    public void run()
    {
    	DatagramSocket socket = null;
        DatagramPacket inPacket = null;
        byte[] inBuf = new byte[DGRAM_LEN];
        try
        {
          //Prepare to join multicast group
          socket = new DatagramSocket(PORT_NO);
          //InetAddress address = InetAddress.getByName(GROUP_ADDR);
          //socket.joinGroup(address);

              while(true)
              {
                    Log.d("WILLEM", "Listening");
                    inPacket = new DatagramPacket(inBuf, inBuf.length);
                    socket.receive(inPacket);
                    String msg = new String(inBuf, 0, inPacket.getLength());
                    if(mClb != null) mClb.OnReceive(msg);
                    Log.d("WILLEM", "From :" + inPacket.getAddress() + " Msg : " + msg);
              }
        }
        catch(Exception ioe)
        {
            System.out.println(ioe);
        }
      }
}

