package com.w2.ledshirt;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;

import com.w2.ambilwarna.AmbilWarnaDialog;
import com.w2.ambilwarna.AmbilWarnaDialog.OnAmbilWarnaListener;
import com.w2.ledshirt.ColorPickerDialog.OnColorChangedListener;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.graphics.Color;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.SeekBar;
import android.widget.ToggleButton;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.Toast;

public class MainActivity extends Activity {

    private static Context mContext;
    private static ReceiverThread th;

	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mContext = this;
        th = new ReceiverThread();
        new Thread(th).start();
        if (savedInstanceState == null) {
            getFragmentManager().beginTransaction()
                    .add(R.id.container, new PlaceholderFragment())
                    .commit();
        }
        
      
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {

        public PlaceholderFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_main, container, false);
            
            rootView.findViewById(R.id.btnRnd).setOnClickListener(new OnClickListener() {
    			@Override
    			public void onClick(View v) {
    				ToggleButton btn = (ToggleButton) v;
    				new SenderTask(th).execute(0x02, btn.isChecked() ? 1 : 0, 0, 0);
    			}
            });
            
            rootView.findViewById(R.id.btnTest).setOnClickListener(new OnClickListener() {
            	private TestSenderTask task;
    			@Override
    			public void onClick(View v) {
    				ToggleButton btn = (ToggleButton) v;
    				if(!btn.isChecked()) {
    					task.mRunning = false;
    				} else {
    					task = new TestSenderTask(th);
    					task.execute(0);	
    				}
    				
    				
    			}
            });
            
            
            
            
            rootView.findViewById(R.id.btnBrightness).setOnClickListener(new OnClickListener() {
    			@Override
    			public void onClick(View v) {
    				ToggleButton btn = (ToggleButton) v;
    				new SenderTask(th).execute(0x06, btn.isChecked() ? 1 : 0, 0, 0);
    			}
            });
            
            rootView.findViewById(R.id.btn).setOnClickListener(new OnClickListener() {
    			@Override
    			public void onClick(View v) {
    				
    				AmbilWarnaDialog dialog = new AmbilWarnaDialog(getActivity(), Color.RED, new OnAmbilWarnaListener() {
						
						@Override
						public void onOk(AmbilWarnaDialog dialog, int color) {
							Log.d("WILLEM", "Sending:" + Color.red(color) + " - " + Color.green(color) + " - " + Color.blue(color));
							
							new SenderTask(th).execute(0x01, Color.red(color), Color.green(color),  Color.blue(color));
						}
						
						@Override
						public void onCancel(AmbilWarnaDialog dialog) {
							// TODO Auto-generated method stub
							
						}
					});
    				dialog.show();
    				// new ColorPickerDialog(getActivity(), (MainActivity) getActivity(), 0).show();
    				 //
    			}
    		});
            
            SeekBar skbr = (SeekBar) rootView.findViewById(R.id.seekBarBrightnessSpeed);
            skbr.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
				
				@Override
				public void onStopTrackingTouch(SeekBar seekBar) {
					// TODO Auto-generated method stub
					new SenderTask(th).execute(0x07, seekBar.getProgress(), 0,0);
				}
				
				@Override
				public void onStartTrackingTouch(SeekBar seekBar) {
					// TODO Auto-generated method stub
					//new SenderTask().execute(0x02, seekBar.getProgress(), 0,0);
				}
				
				@Override
				public void onProgressChanged(SeekBar seekBar, int progress,
						boolean fromUser) {
					// TODO Auto-generated method stub
					
				}
			});
            
            
            SeekBar skcr = (SeekBar) rootView.findViewById(R.id.seekBarColorRandomSpeed);
            skcr.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
				
				@Override
				public void onStopTrackingTouch(SeekBar seekBar) {
					// TODO Auto-generated method stub
					new SenderTask(th).execute(0x03, seekBar.getProgress(), 0,0);
				}
				
				@Override
				public void onStartTrackingTouch(SeekBar seekBar) {
					// TODO Auto-generated method stub
					//new SenderTask().execute(0x02, seekBar.getProgress(), 0,0);
				}
				
				@Override
				public void onProgressChanged(SeekBar seekBar, int progress,
						boolean fromUser) {
					// TODO Auto-generated method stub
					
				}
			});
            
            
            
            SeekBar sk = (SeekBar) rootView.findViewById(R.id.seekBar1);
            sk.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
				
				@Override
				public void onStopTrackingTouch(SeekBar seekBar) {
					// TODO Auto-generated method stub
					new SenderTask(th).execute(0x04, seekBar.getProgress(), 0,0);
				}
				
				@Override
				public void onStartTrackingTouch(SeekBar seekBar) {
					// TODO Auto-generated method stub
					//new SenderTask().execute(0x02, seekBar.getProgress(), 0,0);
				}
				
				@Override
				public void onProgressChanged(SeekBar seekBar, int progress,
						boolean fromUser) {
					// TODO Auto-generated method stub
					
				}
			});
            
            SeekBar sk2 = (SeekBar) rootView.findViewById(R.id.seekBar2);
            sk2.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
				
				@Override
				public void onStopTrackingTouch(SeekBar seekBar) {
					// TODO Auto-generated method stub
					new SenderTask(th).execute(0x05, seekBar.getProgress(), 0,0);
				}
				
				@Override
				public void onStartTrackingTouch(SeekBar seekBar) {
					// TODO Auto-generated method stub
					//new SenderTask().execute(0x02, seekBar.getProgress(), 0,0);
				}
				
				@Override
				public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
					// TODO Auto-generated method stub
					
				}
			});
            
            return rootView;
        }
    }
}
